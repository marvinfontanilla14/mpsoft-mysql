package com.mpsoft.springmvc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "user_level_settings")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@userLevelId")
public class UserLevelSetting implements Serializable {

	private static final long serialVersionUID = 1338050456437705028L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_level_id")
	private int userLevelID;

	@Column(name = "user_role")
	private int userRole;

	@Column(name = "is_active_resources")
	private boolean isActiveResources;

	@Column(name = "is_active_crud")
	private boolean isActiveCrud;

	@Column(name = "is_active_jasper")
	private boolean isActiveJasper;

	@Column(name = "is_active_utilities")
	private boolean isActiveUtilities;

	public int getUserLevelID() {
		return userLevelID;
	}

	public void setUserLevelID(int userLevelID) {
		this.userLevelID = userLevelID;
	}

	public int getUserRole() {
		return userRole;
	}

	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}

	public boolean isActiveResources() {
		return isActiveResources;
	}

	public void setActiveResources(boolean isActiveResources) {
		this.isActiveResources = isActiveResources;
	}

	public boolean isActiveCrud() {
		return isActiveCrud;
	}

	public void setActiveCrud(boolean isActiveCrud) {
		this.isActiveCrud = isActiveCrud;
	}

	public boolean isActiveJasper() {
		return isActiveJasper;
	}

	public void setActiveJasper(boolean isActiveJasper) {
		this.isActiveJasper = isActiveJasper;
	}

	public boolean isActiveUtilities() {
		return isActiveUtilities;
	}

	public void setActiveUtilities(boolean isActiveUtilities) {
		this.isActiveUtilities = isActiveUtilities;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isActiveCrud ? 1231 : 1237);
		result = prime * result + (isActiveJasper ? 1231 : 1237);
		result = prime * result + (isActiveResources ? 1231 : 1237);
		result = prime * result + (isActiveUtilities ? 1231 : 1237);
		result = prime * result + userLevelID;
		result = prime * result + userRole;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserLevelSetting other = (UserLevelSetting) obj;
		if (isActiveCrud != other.isActiveCrud)
			return false;
		if (isActiveJasper != other.isActiveJasper)
			return false;
		if (isActiveResources != other.isActiveResources)
			return false;
		if (isActiveUtilities != other.isActiveUtilities)
			return false;
		if (userLevelID != other.userLevelID)
			return false;
		if (userRole != other.userRole)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserLevelSetting [userLevelID=" + userLevelID + ", userRole="
				+ userRole + ", isActiveResources=" + isActiveResources
				+ ", isActiveCrud=" + isActiveCrud + ", isActiveJasper="
				+ isActiveJasper + ", isActiveUtilities=" + isActiveUtilities
				+ "]";
	}

}
