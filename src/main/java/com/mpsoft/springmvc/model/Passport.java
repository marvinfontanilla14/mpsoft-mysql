package com.mpsoft.springmvc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

//import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mpsoft.springmvc.utilities.CustomDateSerializer;

@Entity
@Table(name = "passport")
public class Passport implements Serializable {

	private static final long serialVersionUID = 8912717531791534329L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "passport_id")
	private int passportID;

	@Size(max = 25)
	@Column(name = "passport_no", nullable = false)
	private String passportNo;

	@Column(name = "issued_date", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date issuedDate;

	@Column(name = "valid_til", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date validTil;

	public int getPassportID() {
		return passportID;
	}

	public void setPassportID(int passportID) {
		this.passportID = passportID;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public Date getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}

	public Date getValidTil() {
		return validTil;
	}

	public void setValidTil(Date validTil) {
		this.validTil = validTil;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((issuedDate == null) ? 0 : issuedDate.hashCode());
		result = prime * result + passportID;
		result = prime * result
				+ ((passportNo == null) ? 0 : passportNo.hashCode());
		result = prime * result
				+ ((validTil == null) ? 0 : validTil.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Passport other = (Passport) obj;
		if (issuedDate == null) {
			if (other.issuedDate != null)
				return false;
		} else if (!issuedDate.equals(other.issuedDate))
			return false;
		if (passportID != other.passportID)
			return false;
		if (passportNo == null) {
			if (other.passportNo != null)
				return false;
		} else if (!passportNo.equals(other.passportNo))
			return false;
		if (validTil == null) {
			if (other.validTil != null)
				return false;
		} else if (!validTil.equals(other.validTil))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Passport [passportID=" + passportID + ", passportNo="
				+ passportNo + ", issuedDate=" + issuedDate + ", validTil="
				+ validTil + "]";
	}

}
