package com.mpsoft.springmvc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

//import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mpsoft.springmvc.utilities.CustomDateSerializer;
import com.mpsoft.springmvc.utilities.CustomTimeSerializer;

@Entity
@Table(name = "persons")
public class Person implements Serializable {

	private static final long serialVersionUID = 3264429667071845982L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "person_id")
	private int personID;

	@OneToOne
	@JoinColumn(name = "passport_id")
	private Passport passport;

	@Size(max = 25)
	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Size(max = 25)
	@Column(name = "middle_name")
	private String middleName;

	@Size(max = 25)
	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "dob", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date dob;

	@Column(name = "pob", nullable = false)
	@Type(type = "text")
	private String pob;

	@Column(name = "date_registered", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date dateRegist;

	@Column(name = "time_registered", nullable = false)
	@Type(type = "time")
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date timeRegist;

	public int getPersonID() {
		return personID;
	}

	public void setPersonID(int personID) {
		this.personID = personID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getPob() {
		return pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Passport getPassport() {
		return passport;
	}

	public void setPassport(Passport passport) {
		this.passport = passport;
	}

	public Date getDateRegist() {
		return dateRegist;
	}

	public void setDateRegist(Date dateRegist) {
		this.dateRegist = dateRegist;
	}

	public Date getTimeRegist() {
		return timeRegist;
	}

	public void setTimeRegist(Date timeRegist) {
		this.timeRegist = timeRegist;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateRegist == null) ? 0 : dateRegist.hashCode());
		result = prime * result + ((dob == null) ? 0 : dob.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result
				+ ((passport == null) ? 0 : passport.hashCode());
		result = prime * result + personID;
		result = prime * result + ((pob == null) ? 0 : pob.hashCode());
		result = prime * result
				+ ((timeRegist == null) ? 0 : timeRegist.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (dateRegist == null) {
			if (other.dateRegist != null)
				return false;
		} else if (!dateRegist.equals(other.dateRegist))
			return false;
		if (dob == null) {
			if (other.dob != null)
				return false;
		} else if (!dob.equals(other.dob))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (passport == null) {
			if (other.passport != null)
				return false;
		} else if (!passport.equals(other.passport))
			return false;
		if (personID != other.personID)
			return false;
		if (pob == null) {
			if (other.pob != null)
				return false;
		} else if (!pob.equals(other.pob))
			return false;
		if (timeRegist == null) {
			if (other.timeRegist != null)
				return false;
		} else if (!timeRegist.equals(other.timeRegist))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Person [personID=" + personID + ", passport=" + passport
				+ ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", dob=" + dob + ", pob=" + pob
				+ ", dateRegist=" + dateRegist + ", timeRegist=" + timeRegist
				+ "]";
	}

}
