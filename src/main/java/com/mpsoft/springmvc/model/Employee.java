package com.mpsoft.springmvc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mpsoft.springmvc.utilities.CustomDateSerializer;
import com.mpsoft.springmvc.utilities.CustomTimeSerializer;

@Entity
@Table(name = "employees")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@employeeId")
public class Employee implements Serializable {

	private static final long serialVersionUID = -4450822924870262576L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_id")
	private int empID;

	@ManyToMany(mappedBy = "employeeList", fetch = FetchType.EAGER)
	private Collection<Project> projectList = new ArrayList<Project>();

	@Size(max = 15)
	@Column(name = "emp_no", nullable = false)
	private String empNo;

	@Size(max = 25)
	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Size(max = 25)
	@Column(name = "middle_name")
	private String middleName;

	@Size(max = 25)
	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Size(max = 25)
	@Column(name = "job_position", nullable = false)
	private String jobPosition;

	@Column(name = "date_hired", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date dateHired;

	@Column(name = "date_created", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date dateCreated;

	@Column(name = "time_created", nullable = false)
	@Type(type = "time")
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date timeCreated;

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJobPosition() {
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}

	public Date getDateHired() {
		return dateHired;
	}

	public void setDateHired(Date dateHired) {
		this.dateHired = dateHired;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Collection<Project> getProject() {
		return projectList;
	}

	public void setProject(Collection<Project> projectList) {
		this.projectList = projectList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result
				+ ((dateHired == null) ? 0 : dateHired.hashCode());
		result = prime * result + empID;
		result = prime * result + ((empNo == null) ? 0 : empNo.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((jobPosition == null) ? 0 : jobPosition.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result
				+ ((projectList == null) ? 0 : projectList.hashCode());
		result = prime * result
				+ ((timeCreated == null) ? 0 : timeCreated.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (dateHired == null) {
			if (other.dateHired != null)
				return false;
		} else if (!dateHired.equals(other.dateHired))
			return false;
		if (empID != other.empID)
			return false;
		if (empNo == null) {
			if (other.empNo != null)
				return false;
		} else if (!empNo.equals(other.empNo))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (jobPosition == null) {
			if (other.jobPosition != null)
				return false;
		} else if (!jobPosition.equals(other.jobPosition))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (projectList == null) {
			if (other.projectList != null)
				return false;
		} else if (!projectList.equals(other.projectList))
			return false;
		if (timeCreated == null) {
			if (other.timeCreated != null)
				return false;
		} else if (!timeCreated.equals(other.timeCreated))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Employee [empID=" + empID + ", empNo=" + empNo + ", firstName="
				+ firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", jobPosition=" + jobPosition + ", dateHired="
				+ dateHired + ", dateCreated=" + dateCreated + ", timeCreated="
				+ timeCreated + "]";
	}

}
