package com.mpsoft.springmvc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mpsoft.springmvc.utilities.CustomDateSerializer;
import com.mpsoft.springmvc.utilities.CustomTimeSerializer;

@Entity
@Table(name = "products")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@productId")
public class Product implements Serializable {

	private static final long serialVersionUID = 1676603552294113881L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "prod_id")
	private int prodID;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ctgy_id")
	private Category category;

	@Size(max = 15)
	@Column(name = "prod_code", nullable = false)
	private String prodCode;

	@Size(max = 100)
	@Column(name = "prod_name", nullable = false)
	private String prodName;

	@Column(name = "prod_cost")
	private Double prodCost;

	@Column(name = "selling_price")
	private Double sellingPrice;

	@Column(name = "order_point")
	private Double orderPoint;

	@Column(name = "date_created", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date dateCreated;

	@Column(name = "time_created", nullable = false)
	@Type(type = "time")
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date timeCreated;

	public int getProdID() {
		return prodID;
	}

	public void setProdID(int prodID) {
		this.prodID = prodID;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Double getProdCost() {
		return prodCost;
	}

	public void setProdCost(Double prodCost) {
		this.prodCost = prodCost;
	}

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public Double getOrderPoint() {
		return orderPoint;
	}

	public void setOrderPoint(Double orderPoint) {
		this.orderPoint = orderPoint;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result
				+ ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result
				+ ((orderPoint == null) ? 0 : orderPoint.hashCode());
		result = prime * result
				+ ((prodCode == null) ? 0 : prodCode.hashCode());
		result = prime * result
				+ ((prodCost == null) ? 0 : prodCost.hashCode());
		result = prime * result + prodID;
		result = prime * result
				+ ((prodName == null) ? 0 : prodName.hashCode());
		result = prime * result
				+ ((sellingPrice == null) ? 0 : sellingPrice.hashCode());
		result = prime * result
				+ ((timeCreated == null) ? 0 : timeCreated.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (orderPoint == null) {
			if (other.orderPoint != null)
				return false;
		} else if (!orderPoint.equals(other.orderPoint))
			return false;
		if (prodCode == null) {
			if (other.prodCode != null)
				return false;
		} else if (!prodCode.equals(other.prodCode))
			return false;
		if (prodCost == null) {
			if (other.prodCost != null)
				return false;
		} else if (!prodCost.equals(other.prodCost))
			return false;
		if (prodID != other.prodID)
			return false;
		if (prodName == null) {
			if (other.prodName != null)
				return false;
		} else if (!prodName.equals(other.prodName))
			return false;
		if (sellingPrice == null) {
			if (other.sellingPrice != null)
				return false;
		} else if (!sellingPrice.equals(other.sellingPrice))
			return false;
		if (timeCreated == null) {
			if (other.timeCreated != null)
				return false;
		} else if (!timeCreated.equals(other.timeCreated))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Product [prodID=" + prodID + ", prodCode=" + prodCode
				+ ", prodName=" + prodName + ", prodCost=" + prodCost
				+ ", sellingPrice=" + sellingPrice + ", orderPoint="
				+ orderPoint + ", dateCreated=" + dateCreated
				+ ", timeCreated=" + timeCreated + "]";
	}

}
