package com.mpsoft.springmvc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "gamers")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@gamerId")
public class Gamer implements Serializable {

	private static final long serialVersionUID = -513481739235130197L;

	@Id
	@Column(name = "gamer_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int gamerID;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "gamers_games", joinColumns = @JoinColumn(name = "gamer_id"), inverseJoinColumns = @JoinColumn(name = "game_id"))
	private Collection<Game> gameList = new ArrayList<Game>();

	@Size(max = 30)
	@Column(name = "gamer_name", nullable = false)
	private String gamerName;

	public int getGamerID() {
		return gamerID;
	}

	public void setGamerID(int gamerID) {
		this.gamerID = gamerID;
	}

	public Collection<Game> getGameList() {
		return gameList;
	}

	public void setGameList(Collection<Game> gameList) {
		this.gameList = gameList;
	}

	public String getGamerName() {
		return gamerName;
	}

	public void setGamerName(String gamerName) {
		this.gamerName = gamerName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((gameList == null) ? 0 : gameList.hashCode());
		result = prime * result + gamerID;
		result = prime * result
				+ ((gamerName == null) ? 0 : gamerName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gamer other = (Gamer) obj;
		if (gameList == null) {
			if (other.gameList != null)
				return false;
		} else if (!gameList.equals(other.gameList))
			return false;
		if (gamerID != other.gamerID)
			return false;
		if (gamerName == null) {
			if (other.gamerName != null)
				return false;
		} else if (!gamerName.equals(other.gamerName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Gamer [gamerID=" + gamerID + ", gamerName=" + gamerName + "]";
	}

}
