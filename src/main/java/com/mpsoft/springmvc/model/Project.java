package com.mpsoft.springmvc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mpsoft.springmvc.utilities.CustomDateSerializer;
import com.mpsoft.springmvc.utilities.CustomTimeSerializer;

@Entity
@Table(name = "projects")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@projectId")
public class Project implements Serializable {

	private static final long serialVersionUID = 6223544071878445371L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "proj_id")
	private int projID;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "projects_employees", joinColumns = @JoinColumn(name = "proj_id"), inverseJoinColumns = @JoinColumn(name = "emp_id"))
	private Collection<Employee> employeeList = new ArrayList<Employee>();

	@Size(max = 15)
	@Column(name = "proj_code", nullable = false)
	private String projCode;

	@Size(max = 50)
	@Column(name = "proj_ctgy", nullable = false)
	private String projCtgy;

	@Size(max = 100)
	@Column(name = "proj_name", nullable = false)
	private String projName;

	@Column(name = "date_start", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date dateStart;

	@Column(name = "date_accmphld", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date dateAccmphld;

	@Column(name = "date_created", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date dateCreated;

	@Column(name = "time_created", nullable = false)
	@Type(type = "time")
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date timeCreated;

	public int getProjID() {
		return projID;
	}

	public void setProjID(int projID) {
		this.projID = projID;
	}

	public String getProjCode() {
		return projCode;
	}

	public void setProjCode(String projCode) {
		this.projCode = projCode;
	}

	public String getProjCtgy() {
		return projCtgy;
	}

	public void setProjCtgy(String projCtgy) {
		this.projCtgy = projCtgy;
	}

	public String getProjName() {
		return projName;
	}

	public void setProjName(String projName) {
		this.projName = projName;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateAccmphld() {
		return dateAccmphld;
	}

	public void setDateAccmphld(Date dateAccmphld) {
		this.dateAccmphld = dateAccmphld;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Collection<Employee> getEmployee() {
		return employeeList;
	}

	public void setEmployee(Collection<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateAccmphld == null) ? 0 : dateAccmphld.hashCode());
		result = prime * result
				+ ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result
				+ ((dateStart == null) ? 0 : dateStart.hashCode());
		result = prime * result
				+ ((employeeList == null) ? 0 : employeeList.hashCode());
		result = prime * result
				+ ((projCode == null) ? 0 : projCode.hashCode());
		result = prime * result
				+ ((projCtgy == null) ? 0 : projCtgy.hashCode());
		result = prime * result + projID;
		result = prime * result
				+ ((projName == null) ? 0 : projName.hashCode());
		result = prime * result
				+ ((timeCreated == null) ? 0 : timeCreated.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (dateAccmphld == null) {
			if (other.dateAccmphld != null)
				return false;
		} else if (!dateAccmphld.equals(other.dateAccmphld))
			return false;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (dateStart == null) {
			if (other.dateStart != null)
				return false;
		} else if (!dateStart.equals(other.dateStart))
			return false;
		if (employeeList == null) {
			if (other.employeeList != null)
				return false;
		} else if (!employeeList.equals(other.employeeList))
			return false;
		if (projCode == null) {
			if (other.projCode != null)
				return false;
		} else if (!projCode.equals(other.projCode))
			return false;
		if (projCtgy == null) {
			if (other.projCtgy != null)
				return false;
		} else if (!projCtgy.equals(other.projCtgy))
			return false;
		if (projID != other.projID)
			return false;
		if (projName == null) {
			if (other.projName != null)
				return false;
		} else if (!projName.equals(other.projName))
			return false;
		if (timeCreated == null) {
			if (other.timeCreated != null)
				return false;
		} else if (!timeCreated.equals(other.timeCreated))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Project [projID=" + projID + ", projCode=" + projCode
				+ ", projCtgy=" + projCtgy + ", projName=" + projName
				+ ", dateStart=" + dateStart + ", dateAccmphld=" + dateAccmphld
				+ ", dateCreated=" + dateCreated + ", timeCreated="
				+ timeCreated + "]";
	}

}
