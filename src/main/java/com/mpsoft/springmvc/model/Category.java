package com.mpsoft.springmvc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mpsoft.springmvc.utilities.CustomDateSerializer;
import com.mpsoft.springmvc.utilities.CustomTimeSerializer;

@Entity
@Table(name = "categories")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@categoryId")
public class Category implements Serializable {

	private static final long serialVersionUID = -5069503785397064679L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ctgy_id")
	private int ctgyID;

	@OneToMany(mappedBy = "category", fetch = FetchType.EAGER)
	private Collection<Product> product = new ArrayList<Product>();

	@Size(max = 25)
	@Column(name = "ctgy_name", nullable = false)
	private String ctgyName;

	@Column(name = "date_created", nullable = false)
	@Type(type = "date")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date dateCreated;

	@Column(name = "time_created", nullable = false)
	@Type(type = "time")
	@JsonSerialize(using = CustomTimeSerializer.class)
	private Date timeCreated;

	public int getCtgyID() {
		return ctgyID;
	}

	public void setCtgyID(int ctgyID) {
		this.ctgyID = ctgyID;
	}

	public String getCtgyName() {
		return ctgyName;
	}

	public void setCtgyName(String ctgyName) {
		this.ctgyName = ctgyName;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Collection<Product> getProduct() {
		return product;
	}

	public void setProduct(Collection<Product> product) {
		this.product = product;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ctgyID;
		result = prime * result
				+ ((ctgyName == null) ? 0 : ctgyName.hashCode());
		result = prime * result
				+ ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result
				+ ((timeCreated == null) ? 0 : timeCreated.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (ctgyID != other.ctgyID)
			return false;
		if (ctgyName == null) {
			if (other.ctgyName != null)
				return false;
		} else if (!ctgyName.equals(other.ctgyName))
			return false;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (timeCreated == null) {
			if (other.timeCreated != null)
				return false;
		} else if (!timeCreated.equals(other.timeCreated))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Category [ctgyID=" + ctgyID + ", ctgyName=" + ctgyName
				+ ", dateCreated=" + dateCreated + ", timeCreated="
				+ timeCreated + "]";
	}

}
