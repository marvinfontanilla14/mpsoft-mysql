package com.mpsoft.springmvc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "games")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@gameId")
public class Game implements Serializable {

	private static final long serialVersionUID = 6661557559980160794L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "game_id")
	private int gameID;

	@ManyToMany(mappedBy = "gameList", fetch = FetchType.EAGER)
	private Collection<Gamer> gamerList = new ArrayList<Gamer>();

	@Size(max = 30)
	@Column(name = "game_name", nullable = false)
	private String gameName;

	public int getGameID() {
		return gameID;
	}

	public void setGameID(int gameID) {
		this.gameID = gameID;
	}

	public Collection<Gamer> getGamerList() {
		return gamerList;
	}

	public void setGamerList(Collection<Gamer> gamerList) {
		this.gamerList = gamerList;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + gameID;
		result = prime * result
				+ ((gameName == null) ? 0 : gameName.hashCode());
		result = prime * result
				+ ((gamerList == null) ? 0 : gamerList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (gameID != other.gameID)
			return false;
		if (gameName == null) {
			if (other.gameName != null)
				return false;
		} else if (!gameName.equals(other.gameName))
			return false;
		if (gamerList == null) {
			if (other.gamerList != null)
				return false;
		} else if (!gamerList.equals(other.gamerList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Game [gameID=" + gameID + ", gameName=" + gameName + "]";
	}

}
