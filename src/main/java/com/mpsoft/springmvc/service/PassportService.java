package com.mpsoft.springmvc.service;

import java.util.List;

import com.mpsoft.springmvc.model.Passport;

public interface PassportService {

	void savePassport(Passport passport);

	List<Passport> passportList();

	void deletePassport(Passport passport);

	List<Passport> getPassportByID(int passportID);

	void updatePassport(Passport passport);

	int latestPassportID();

}
