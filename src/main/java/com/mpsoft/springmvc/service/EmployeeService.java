package com.mpsoft.springmvc.service;

import java.util.List;
import java.util.Map;

import com.mpsoft.springmvc.model.Employee;

public interface EmployeeService {

	void saveEmployee(Employee employee);

	List<Map<String, Object>> employeeList();

	void deleteEmployee(int empID);

	List<Map<String, Object>> getEmployeeByID(int empID);

	List<Employee> getEmployeeBeanByID(int empID);

	void updateEmployee(Employee employee);

	boolean isEmployeeAvailable(String firstName, String middleName,
			String lastName, String jobPosition);

	boolean isEmployeeDetailsChange(String firstName, String middleName,
			String lastName, String jobPosition, int empID);

	public boolean canBeDeleted(int empID);

	int latestEmployeeID();

}
