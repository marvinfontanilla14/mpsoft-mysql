package com.mpsoft.springmvc.service;

import java.util.List;

import com.mpsoft.springmvc.model.UserLevelSetting;

public interface UserLevelSettingService {

	List<UserLevelSetting> userLevelList();

	List<UserLevelSetting> getUserLevelByRole(int userRole);

	void saveUserLevel(UserLevelSetting userLevel);

	void updateUserLevel(UserLevelSetting userLevel);

	boolean hasRoleSettings(int userRole);
}
