package com.mpsoft.springmvc.service;

import java.util.List;
import java.util.Map;

import com.mpsoft.springmvc.model.Project;

public interface ProjectService {
	void saveProject(Project project);

	List<Map<String, Object>> projectList();

	void deleteProject(int projID);

	List<Map<String, Object>> getProjectByID(int projID);

	List<Project> getProjectBeanByID(int projID);

	void updateProject(Project project);

	boolean isProjectAvailable(String projCode, String projCtgy, String projName);

	boolean isProjectDetailsChange(String projCode, String projCtgy,
			String projName, int projID);

	List<Map<String, Object>> projectEmployeeList();

	public boolean canBeDeleted(int projID);

	int latestProjectID();
}
