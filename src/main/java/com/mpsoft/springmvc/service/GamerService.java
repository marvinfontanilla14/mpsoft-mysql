package com.mpsoft.springmvc.service;

import java.util.List;
import java.util.Map;

import com.mpsoft.springmvc.model.Gamer;

public interface GamerService {

	void saveGamer(Gamer gamer);

	List<Map<String, Object>> gamerList();

	void deleteGamerByID(int gamerID);
}
