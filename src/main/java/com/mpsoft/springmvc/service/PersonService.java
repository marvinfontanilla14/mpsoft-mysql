package com.mpsoft.springmvc.service;

import java.util.List;

import com.mpsoft.springmvc.model.Person;

public interface PersonService {

	void savePerson(Person person);

	List<Person> personList();

	void deletePerson(Person person);

	List<Person> getPersonByID(int personID);

	void updatePerson(Person person);

	boolean isPersonAvailable(String firstName, String middleName,
			String lastName);

	boolean isPersonDetailsChange(String firstName, String middleName,
			String lastName, int personID);
}
