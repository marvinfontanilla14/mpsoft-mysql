package com.mpsoft.springmvc.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mpsoft.springmvc.dao.CategoryDao;
import com.mpsoft.springmvc.model.Category;
import com.mpsoft.springmvc.service.CategoryService;

@Service("categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDao categoryDao;

	public void saveCategory(Category category) {
		categoryDao.saveCategory(category);
	}

	public List<Map<String, Object>> categoryList() {
		return categoryDao.categoryList();
	}

	public void deleteCategory(int ctgyID) {
		categoryDao.deleteCategory(ctgyID);
	}

	public List<Map<String, Object>> getCategoryByID(int ctgyID) {
		return categoryDao.getCategoryByID(ctgyID);
	}

	public void updateCategory(Category category) {
		categoryDao.updateCategory(category);
	}

	public boolean isCategoryAvailable(String ctgyName) {
		return categoryDao.isCategoryAvailable(ctgyName);
	}

	public boolean isCategoryDetailsChange(String ctgyName, int ctgyID) {
		return categoryDao.isCategoryDetailsChange(ctgyName, ctgyID);
	}

	public boolean canBeDeleted(int ctgyID) {
		return categoryDao.canBeDeleted(ctgyID);
	}

	public List<Category> getCategoryBeanByID(int ctgyID) {
		return categoryDao.getCategoryBeanByID(ctgyID);
	}

}
