package com.mpsoft.springmvc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mpsoft.springmvc.dao.UserDao;
import com.mpsoft.springmvc.model.User;
import com.mpsoft.springmvc.service.UserService;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	public void saveUser(User user) {
		userDao.saveUser(user);
	}

	public List<User> getUserList() {
		return userDao.getUserList();
	}

	public String isValidUser(String userName) {
		return userDao.isValidUser(userName);
	}

	public void updateUser(User user) {
		userDao.updateUser(user);
	}

	public boolean isUserAvailable(String userName) {
		return userDao.isUserAvailable(userName);
	}

	public boolean isUserDetailsChange(String userName, int userID) {
		return userDao.isUserDetailsChange(userName, userID);
	}

	public List<User> getUserByID(int userID) {
		return userDao.getUserByID(userID);
	}

	public String getUserRoleByUserName(String userName) {
		return userDao.getUserRoleByUserName(userName);
	}

}
