package com.mpsoft.springmvc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mpsoft.springmvc.dao.UserLevelSettingDao;
import com.mpsoft.springmvc.model.UserLevelSetting;
import com.mpsoft.springmvc.service.UserLevelSettingService;

@Service("userLevelSettingService")
@Transactional
public class UserLevelSettingServiceImpl implements UserLevelSettingService {

	@Autowired
	UserLevelSettingDao userLevelSettingDao;

	public List<UserLevelSetting> userLevelList() {
		return userLevelSettingDao.userLevelList();
	}

	public List<UserLevelSetting> getUserLevelByRole(int userRole) {
		return userLevelSettingDao.getUserLevelByRole(userRole);
	}

	public void saveUserLevel(UserLevelSetting userLevel) {
		userLevelSettingDao.saveUserLevel(userLevel);
	}

	public void updateUserLevel(UserLevelSetting userLevel) {
		userLevelSettingDao.updateUserLevel(userLevel);
	}

	public boolean hasRoleSettings(int userRole) {
		return userLevelSettingDao.hasRoleSettings(userRole);
	}

}
