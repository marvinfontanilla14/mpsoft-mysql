package com.mpsoft.springmvc.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mpsoft.springmvc.dao.ProjectDao;
import com.mpsoft.springmvc.model.Project;
import com.mpsoft.springmvc.service.ProjectService;

@Service("projectService")
@Transactional
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectDao projectDao;

	public void saveProject(Project project) {
		projectDao.saveProject(project);
	}

	public List<Map<String, Object>> projectList() {
		return projectDao.projectList();
	}

	public void deleteProject(int projID) {
		projectDao.deleteProject(projID);
	}

	public List<Map<String, Object>> getProjectByID(int projID) {
		return projectDao.getProjectByID(projID);
	}

	public void updateProject(Project project) {
		projectDao.updateProject(project);
	}

	public boolean isProjectAvailable(String projCode, String projCtgy,
			String projName) {
		return projectDao.isProjectAvailable(projCode, projCtgy, projName);
	}

	public boolean isProjectDetailsChange(String projCode, String projCtgy,
			String projName, int projID) {
		return projectDao.isProjectDetailsChange(projCode, projCtgy, projName,
				projID);
	}

	public List<Map<String, Object>> projectEmployeeList() {
		return projectDao.projectEmployeeList();
	}

	public boolean canBeDeleted(int projID) {
		return projectDao.canBeDeleted(projID);
	}

	public int latestProjectID() {
		return projectDao.latestProjectID();
	}

	public List<Project> getProjectBeanByID(int projID) {
		return projectDao.getProjectBeanByID(projID);
	}

}
