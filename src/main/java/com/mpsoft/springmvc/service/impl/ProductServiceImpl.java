package com.mpsoft.springmvc.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mpsoft.springmvc.dao.ProductDao;
import com.mpsoft.springmvc.model.Product;
import com.mpsoft.springmvc.service.ProductService;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDao productDao;

	public void saveProduct(Product product) {
		productDao.saveProduct(product);
	}

	public List<Map<String, Object>> productList() {
		return productDao.productList();
	}

	public void deleteProduct(int prodID) {
		productDao.deleteProduct(prodID);
	}

	public List<Map<String, Object>> getProductByID(int prodID) {
		return productDao.getProductByID(prodID);
	}

	public void updateProduct(Product product) {
		productDao.updateProduct(product);
	}

	public boolean isProductAvailable(int ctgyID, String prodCode,
			String prodName) {
		return productDao.isProductAvailable(ctgyID, prodCode, prodName);
	}

	public boolean isProductDetailsChange(int ctgyID, String prodCode,
			String prodName, int prodID) {
		return productDao.isProductDetailsChange(ctgyID, prodCode, prodName,
				prodID);
	}

	public int latestProductID() {
		return productDao.latestProductID();
	}

}
