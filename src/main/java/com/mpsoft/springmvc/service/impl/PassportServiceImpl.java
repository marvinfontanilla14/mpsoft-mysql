package com.mpsoft.springmvc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mpsoft.springmvc.dao.PassportDao;
import com.mpsoft.springmvc.model.Passport;
import com.mpsoft.springmvc.service.PassportService;

@Service("passportService")
@Transactional
public class PassportServiceImpl implements PassportService {

	@Autowired
	PassportDao passportDao;

	public void savePassport(Passport passport) {
		passportDao.savePassport(passport);
	}

	public List<Passport> passportList() {
		return passportDao.passportList();
	}

	public void deletePassport(Passport passport) {
		passportDao.deletePassport(passport);
	}

	public List<Passport> getPassportByID(int passportID) {
		return passportDao.getPassportByID(passportID);
	}

	public void updatePassport(Passport passport) {
		passportDao.updatePassport(passport);
	}

	public int latestPassportID() {
		return passportDao.latestPassportID();
	}

}
