package com.mpsoft.springmvc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mpsoft.springmvc.dao.PersonDao;
import com.mpsoft.springmvc.model.Person;
import com.mpsoft.springmvc.service.PersonService;

@Service("personService")
@Transactional
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonDao personDao;

	public void savePerson(Person person) {
		personDao.savePerson(person);
	}

	public List<Person> personList() {
		return personDao.personList();
	}

	public void deletePerson(Person person) {
		personDao.deletePerson(person);
	}

	public List<Person> getPersonByID(int personID) {
		return personDao.getPersonByID(personID);
	}

	public void updatePerson(Person person) {
		personDao.updatePerson(person);
	}

	public boolean isPersonAvailable(String firstName, String middleName,
			String lastName) {
		return personDao.isPersonAvailable(firstName, middleName, lastName);
	}

	public boolean isPersonDetailsChange(String firstName, String middleName,
			String lastName, int personID) {
		return personDao.isPersonDetailsChange(firstName, middleName, lastName,
				personID);
	}

}
