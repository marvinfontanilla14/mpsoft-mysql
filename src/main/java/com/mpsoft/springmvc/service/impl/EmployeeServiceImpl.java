package com.mpsoft.springmvc.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mpsoft.springmvc.dao.EmployeeDao;
import com.mpsoft.springmvc.model.Employee;
import com.mpsoft.springmvc.service.EmployeeService;

@Service("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDao employeeDao;

	public void saveEmployee(Employee employee) {
		employeeDao.saveEmployee(employee);
	}

	public List<Map<String, Object>> employeeList() {
		return employeeDao.employeeList();
	}

	public void deleteEmployee(int empID) {
		employeeDao.deleteEmployee(empID);
	}

	public List<Map<String, Object>> getEmployeeByID(int empID) {
		return employeeDao.getEmployeeByID(empID);
	}

	public void updateEmployee(Employee employee) {
		employeeDao.updateEmployee(employee);
	}

	public boolean isEmployeeAvailable(String firstName, String middleName,
			String lastName, String jobPosition) {
		return employeeDao.isEmployeeAvailable(firstName, middleName, lastName,
				jobPosition);
	}

	public boolean isEmployeeDetailsChange(String firstName, String middleName,
			String lastName, String jobPosition, int empID) {
		return employeeDao.isEmployeeDetailsChange(firstName, middleName,
				lastName, jobPosition, empID);
	}

	public int latestEmployeeID() {
		return employeeDao.latestEmployeeID();
	}

	public boolean canBeDeleted(int empID) {
		return employeeDao.canBeDeleted(empID);
	}

	public List<Employee> getEmployeeBeanByID(int empID) {
		return employeeDao.getEmployeeBeanByID(empID);
	}

}
