package com.mpsoft.springmvc.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mpsoft.springmvc.dao.GamerDao;
import com.mpsoft.springmvc.model.Gamer;
import com.mpsoft.springmvc.service.GamerService;

@Service("gamerService")
@Transactional
public class GamerServiceImpl implements GamerService {

	@Autowired
	private GamerDao gamerDao;

	public void saveGamer(Gamer gamer) {
		gamerDao.saveGamer(gamer);
	}

	public List<Map<String, Object>> gamerList() {
		return gamerDao.gamerList();
	}

	public void deleteGamerByID(int gamerID) {
		gamerDao.deleteGamerByID(gamerID);
	}

}
