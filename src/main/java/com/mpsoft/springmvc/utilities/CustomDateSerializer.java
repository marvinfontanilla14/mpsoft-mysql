package com.mpsoft.springmvc.utilities;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.databind.JsonSerializer;

public class CustomDateSerializer extends JsonSerializer<Date> {

	@Override
	public void serialize(Date arg0,
			com.fasterxml.jackson.core.JsonGenerator arg1,
			com.fasterxml.jackson.databind.SerializerProvider arg2)
			throws IOException,
			com.fasterxml.jackson.core.JsonProcessingException {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String formattedDate = formatter.format(arg0);

		arg1.writeString(formattedDate);

	}
}