package com.mpsoft.springmvc.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UIFunctions {

	public String getDateTime(String strDateFormat) {
		DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
		Date date = new Date();
		return dateFormat.format(date);
	}

}
