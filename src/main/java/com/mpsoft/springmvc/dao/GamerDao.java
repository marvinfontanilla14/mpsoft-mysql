package com.mpsoft.springmvc.dao;

import java.util.List;
import java.util.Map;

import com.mpsoft.springmvc.model.Gamer;

public interface GamerDao {
	
	void saveGamer(Gamer gamer);
	
	List<Map<String,Object>> gamerList();
	
	void deleteGamerByID(int gamerID);
 
}
