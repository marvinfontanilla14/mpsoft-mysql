package com.mpsoft.springmvc.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mpsoft.springmvc.dao.AbstractDao;
import com.mpsoft.springmvc.dao.GamerDao;
import com.mpsoft.springmvc.model.Gamer;

@Repository("gamerDao")
public class GamerDaoImpl extends AbstractDao implements GamerDao {

	public void saveGamer(Gamer gamer) {
		persist(gamer);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> gamerList() {
		String HQL_QUERY = "select new map(gr.gamerID as gamerID,"
				+ "gr.gamerName as gamerName,gm.gameID as gameID,"
				+ "gm.gameName as gameName) "
				+ "from Gamer gr join gr.gameList gm ";
		return getSession().createQuery(HQL_QUERY).list();
	}

	public void deleteGamerByID(int gamerID) {
		Criteria criteria = getSession().createCriteria(Gamer.class).add(
				Restrictions.eq("gamerID", gamerID));
		delete((Gamer) criteria.uniqueResult());
	}

}
