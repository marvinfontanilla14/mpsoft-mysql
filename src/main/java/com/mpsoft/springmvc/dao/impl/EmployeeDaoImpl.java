package com.mpsoft.springmvc.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mpsoft.springmvc.dao.AbstractDao;
import com.mpsoft.springmvc.dao.EmployeeDao;
import com.mpsoft.springmvc.model.Employee;

@Repository("employeeDao")
public class EmployeeDaoImpl extends AbstractDao implements EmployeeDao {

	public void saveEmployee(Employee employee) {
		persist(employee);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> employeeList() {
		String HQL_QUERY = "select "
				+ "new map(e.empID as empID,e.empNo as empNo,"
				+ "e.firstName as firstName,e.middleName as middleName,e.lastName as lastName,"
				+ "e.jobPosition as jobPosition,e.dateHired as dateHired) "
				+ "from Employee e";
		return getSession().createQuery(HQL_QUERY).list();
	}

	public void deleteEmployee(int empID) {
		Criteria criteria = getSession().createCriteria(Employee.class).add(
				Restrictions.eq("empID", empID));
		Employee employee = (Employee) criteria.uniqueResult();
		delete(employee);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getEmployeeByID(int empID) {
		String HQL_QUERY = "select "
				+ "new map(e.empID as empID,e.empNo as empNo,"
				+ "e.firstName as firstName,e.middleName as middleName,e.lastName as lastName,"
				+ "e.jobPosition as jobPosition,e.dateHired as dateHired,e.dateCreated as dateCreated,"
				+ "e.timeCreated as timeCreated) "
				+ "from Employee e where e.empID =:empID";
		Query query = getSession().createQuery(HQL_QUERY);
		query.setParameter("empID", empID);
		return query.list();
	}

	public void updateEmployee(Employee employee) {
		update(employee);
	}

	public boolean isEmployeeAvailable(String firstName, String middleName,
			String lastName, String jobPosition) {
		Criteria criteria = getSession().createCriteria(Employee.class)
				.setProjection(Projections.property("empID"))
				.add(Restrictions.eq("firstName", firstName))
				.add(Restrictions.eq("middleName", middleName))
				.add(Restrictions.eq("lastName", lastName))
				.add(Restrictions.eq("jobPosition", jobPosition));
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	public boolean isEmployeeDetailsChange(String firstName, String middleName,
			String lastName, String jobPosition, int empID) {
		Criteria criteria = getSession().createCriteria(Employee.class)
				.setProjection(Projections.property("empID"))
				.add(Restrictions.eq("firstName", firstName))
				.add(Restrictions.eq("middleName", middleName))
				.add(Restrictions.eq("lastName", lastName))
				.add(Restrictions.eq("jobPosition", jobPosition))
				.add(Restrictions.eq("empID", empID));
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	public int latestEmployeeID() {
		Criteria criteria = getSession().createCriteria(Employee.class)
				.setProjection(Property.forName("empID"))
				.addOrder(Order.desc("empID")).setMaxResults(1);
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	public boolean canBeDeleted(int empID) {
		Criteria criteria = getSession().createCriteria(Employee.class, "e")
				.setFetchMode("e.projectList", FetchMode.JOIN)
				.createAlias("e.projectList", "p")
				.setProjection(Projections.property("empID"))
				.add(Restrictions.eq("empID", empID)).setMaxResults(1);
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public List<Employee> getEmployeeBeanByID(int empID) {
		Criteria criteria = getSession().createCriteria(Employee.class).add(
				Restrictions.eq("empID", empID));
		return (List<Employee>) criteria.list();
	}

}
