package com.mpsoft.springmvc.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mpsoft.springmvc.dao.AbstractDao;
import com.mpsoft.springmvc.dao.CategoryDao;
import com.mpsoft.springmvc.model.Category;

@Repository("categoryDao")
public class CategoryDaoImpl extends AbstractDao implements CategoryDao {

	public void saveCategory(Category category) {
		persist(category);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> categoryList() {
		String HQL_QUERY = "select "
				+ "new map(c.ctgyID as ctgyID,c.ctgyName as ctgyName,"
				+ "c.dateCreated as dateCreated,c.timeCreated as timeCreated) "
				+ "from Category c";
		return getSession().createQuery(HQL_QUERY).list();
	}

	public void deleteCategory(int ctgyID) {
		Criteria criteria = getSession().createCriteria(Category.class).add(
				Restrictions.eq("ctgyID", ctgyID));
		Category category = (Category) criteria.uniqueResult();
		delete(category);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getCategoryByID(int ctgyID) {
		String HQL_QUERY = "select "
				+ "new map(c.ctgyID as ctgyID,c.ctgyName as ctgyName,"
				+ "c.dateCreated as dateCreated,c.timeCreated as timeCreated) "
				+ "from Category c where c.ctgyID =:ctgyID";
		Query query = getSession().createQuery(HQL_QUERY);
		query.setParameter("ctgyID", ctgyID);
		return query.list();
	}

	public void updateCategory(Category category) {
		update(category);
	}

	public boolean isCategoryAvailable(String ctgyName) {
		Criteria criteria = getSession().createCriteria(Category.class)
				.setProjection(Property.forName("ctgyID"))
				.add(Restrictions.eq("ctgyName", ctgyName));
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	public boolean isCategoryDetailsChange(String ctgyName, int ctgyID) {
		Criteria criteria = getSession().createCriteria(Category.class)
				.setProjection(Property.forName("ctgyID"))
				.add(Restrictions.eq("ctgyName", ctgyName))
				.add(Restrictions.eq("ctgyID", ctgyID));
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	public boolean canBeDeleted(int ctgyID) {
		Criteria criteria = getSession().createCriteria(Category.class)
				.setFetchMode("product", FetchMode.JOIN)
				.createAlias("product", "p")
				.setProjection(Projections.property("ctgyID"))
				.add(Restrictions.eq("ctgyID", ctgyID)).setMaxResults(1);
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public List<Category> getCategoryBeanByID(int ctgyID) {
		Criteria criteria = getSession().createCriteria(Category.class).add(
				Restrictions.eq("ctgyID", ctgyID));
		return (List<Category>) criteria.list();
	}

}
