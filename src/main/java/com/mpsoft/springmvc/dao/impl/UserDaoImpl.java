package com.mpsoft.springmvc.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mpsoft.springmvc.dao.AbstractDao;
import com.mpsoft.springmvc.dao.UserDao;
import com.mpsoft.springmvc.model.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {

	public void saveUser(User user) {
		save(user);
	}

	@SuppressWarnings("unchecked")
	public List<User> getUserList() {
		Criteria criteria = getSession().createCriteria(User.class);
		return (List<User>) criteria.list();
	}

	public void deleteUserByID(String userID) {
		Query query = getSession().createSQLQuery(
				"delete from User where userID = :userID");
		query.setString("userID", userID);
		query.executeUpdate();
	}

	public String isValidUser(String userName) {
		Criteria criteria = getSession().createCriteria(User.class)
				.setProjection(Property.forName("userPassword"))
				.add(Restrictions.eq("userName", userName))
				.add(Restrictions.eq("status", true));
		if (criteria.list().size() > 0) {
			return criteria.list().get(0).toString();
		}
		return "";
	}

	public void updateUser(User user) {
		update(user);
	}

	public boolean isUserAvailable(String userName) {
		Criteria criteria = getSession().createCriteria(User.class)
				.setProjection(Projections.property("userID"))
				.add(Restrictions.eq("userName", userName)).setMaxResults(1);
		if (criteria.uniqueResult() == null) {
			return true;
		}

		return false;
	}

	public boolean isUserDetailsChange(String userName, int userID) {
		Criteria criteria = getSession().createCriteria(User.class)
				.setProjection(Projections.property("userID"))
				.add(Restrictions.eq("userName", userName))
				.add(Restrictions.eq("userID", userID)).setMaxResults(1);
		if (criteria.uniqueResult() == null) {
			return true;
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	public List<User> getUserByID(int userID) {
		Criteria criteria = getSession().createCriteria(User.class).add(
				(Restrictions.eq("userID", userID)));
		return (List<User>) criteria.list();
	}

	public String getUserRoleByUserName(String userName) {
		Criteria criteria = getSession().createCriteria(User.class)
				.setProjection(Projections.property("userRole"))
				.add(Restrictions.eq("userName", userName));
		return criteria.uniqueResult().toString();
	}
}
