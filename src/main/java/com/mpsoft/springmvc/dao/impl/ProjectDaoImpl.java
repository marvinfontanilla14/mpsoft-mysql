package com.mpsoft.springmvc.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mpsoft.springmvc.dao.AbstractDao;
import com.mpsoft.springmvc.dao.ProjectDao;
import com.mpsoft.springmvc.model.Project;

@Repository("projectDao")
public class ProjectDaoImpl extends AbstractDao implements ProjectDao {

	public void saveProject(Project project) {
		save(project);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> projectList() {
		String HQL_QUERY = "select "
				+ "new map(p.projID as projID,p.projCode as projCode,p.projCtgy as projCtgy,p.projName as projName,"
				+ "p.dateStart as dateStart,p.dateAccmphld as dateAccmphld) "
				+ "from Project p";
		return getSession().createQuery(HQL_QUERY).list();
	}

	public void deleteProject(int projID) {
		Criteria criteria = getSession().createCriteria(Project.class).add(
				Restrictions.eq("projID", projID));
		Project project = (Project) criteria.uniqueResult();
		delete(project);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getProjectByID(int projID) {
		String HQL_QUERY = "select "
				+ "new map(p.projID as projID,p.projCode as projCode,p.projCtgy as projCtgy,p.projName as projName,"
				+ "p.dateStart as dateStart,p.dateAccmphld as dateAccmphld,p.dateCreated as dateCreated,p.timeCreated as timeCreated) "
				+ "from Project p where p.projID =:projID";
		Query query = getSession().createQuery(HQL_QUERY);
		query.setParameter("projID", projID);
		return query.list();
	}

	public void updateProject(Project project) {
		update(project);
	}

	public boolean isProjectAvailable(String projCode, String projCtgy,
			String projName) {
		Criteria criteria = getSession().createCriteria(Project.class)
				.setProjection(Property.forName("projID"))
				.add(Restrictions.eq("projCode", projCode))
				.add(Restrictions.eq("projCtgy", projCtgy))
				.add(Restrictions.eq("projName", projName));
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	public boolean isProjectDetailsChange(String projCode, String projCtgy,
			String projName, int projID) {
		Criteria criteria = getSession().createCriteria(Project.class)
				.setProjection(Property.forName("projID"))
				.add(Restrictions.eq("projCode", projCode))
				.add(Restrictions.eq("projCtgy", projCtgy))
				.add(Restrictions.eq("projName", projName))
				.add(Restrictions.eq("projID", projID));
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> projectEmployeeList() {
		String HQL_QUERY = "select "
				+ "new map(p.projID as projID,p.projCode as projCode,p.projCtgy as projCtgy,p.projName as projName,"
				+ "p.dateStart as dateStart,p.dateAccmphld as dateAccmphld,e.empID as empID,e.empNo as empNo,"
				+ "e.firstName as firstName,e.middleName as middleName,e.lastName as lastName,"
				+ "e.jobPosition as jobPosition,e.dateHired as dateHired) "
				+ "from Project p join p.employeeList e";
		return getSession().createQuery(HQL_QUERY).list();
	}

	public boolean canBeDeleted(int projID) {
		Criteria criteria = getSession().createCriteria(Project.class, "p")
				.setFetchMode("p.employeeList", FetchMode.JOIN)
				.createAlias("p.employeeList", "e")
				.setProjection(Projections.property("projID"))
				.add(Restrictions.eq("projID", projID)).setMaxResults(1);
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	public int latestProjectID() {
		Criteria criteria = getSession().createCriteria(Project.class)
				.setProjection(Property.forName("projID"))
				.addOrder(Order.desc("projID")).setMaxResults(1);
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@SuppressWarnings("unchecked")
	public List<Project> getProjectBeanByID(int projID) {
		Criteria criteria = getSession().createCriteria(Project.class).add(
				Restrictions.eq("projID", projID));
		return (List<Project>) criteria.list();
	}

}
