package com.mpsoft.springmvc.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mpsoft.springmvc.dao.AbstractDao;
import com.mpsoft.springmvc.dao.UserLevelSettingDao;
import com.mpsoft.springmvc.model.UserLevelSetting;

@Repository("userLevelSettingDao")
public class UserLevelSettingDaoImpl extends AbstractDao implements
		UserLevelSettingDao {

	@SuppressWarnings("unchecked")
	public List<UserLevelSetting> userLevelList() {
		Criteria criteria = getSession().createCriteria(UserLevelSetting.class);
		return (List<UserLevelSetting>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<UserLevelSetting> getUserLevelByRole(int userRole) {
		Criteria criteria = getSession().createCriteria(UserLevelSetting.class)
				.add(Restrictions.eq("userRole", userRole));
		return (List<UserLevelSetting>) criteria.list();
	}

	public void saveUserLevel(UserLevelSetting userLevel) {
		save(userLevel);
	}

	public void updateUserLevel(UserLevelSetting userLevel) {
		update(userLevel);
	}

	public boolean hasRoleSettings(int userRole) {
		Criteria criteria = getSession().createCriteria(UserLevelSetting.class)
				.setProjection(Projections.property("userRole"))
				.add(Restrictions.eq("userRole", userRole));
		if (criteria.uniqueResult() != null) {
			return true;
		}
		return false;
	}

}
