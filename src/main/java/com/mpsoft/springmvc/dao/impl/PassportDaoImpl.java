package com.mpsoft.springmvc.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mpsoft.springmvc.dao.AbstractDao;
import com.mpsoft.springmvc.dao.PassportDao;
import com.mpsoft.springmvc.model.Passport;

@Repository("passportDao")
public class PassportDaoImpl extends AbstractDao implements PassportDao {

	public void savePassport(Passport passport) {
		persist(passport);
	}

	@SuppressWarnings("unchecked")
	public List<Passport> passportList() {
		Criteria criteria = getSession().createCriteria(Passport.class);
		return (List<Passport>) criteria.list();
	}

	public void deletePassport(Passport passport) {
		delete(passport);
	}

	@SuppressWarnings("unchecked")
	public List<Passport> getPassportByID(int passportID) {
		Criteria criteria = getSession().createCriteria(Passport.class).add(
				Restrictions.eq("passportID", passportID));
		return (List<Passport>) criteria.list();
	}

	public void updatePassport(Passport passport) {
		update(passport);
	}

	public int latestPassportID() {
		Criteria criteria = getSession().createCriteria(Passport.class)
				.setProjection(Property.forName("passportID"))
				.addOrder(Order.desc("passportID")).setMaxResults(1);
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

}
