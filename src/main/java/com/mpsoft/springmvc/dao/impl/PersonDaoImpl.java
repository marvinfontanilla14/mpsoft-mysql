package com.mpsoft.springmvc.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mpsoft.springmvc.dao.AbstractDao;
import com.mpsoft.springmvc.dao.PersonDao;
import com.mpsoft.springmvc.model.Person;

@Repository("personDao")
public class PersonDaoImpl extends AbstractDao implements PersonDao {

	public void savePerson(Person person) {
		persist(person);
	}

	@SuppressWarnings("unchecked")
	public List<Person> personList() {
		Criteria criteria = getSession().createCriteria(Person.class);
		return (List<Person>) criteria.list();

	}

	public void deletePerson(Person person) {
		delete(person);

	}

	@SuppressWarnings("unchecked")
	public List<Person> getPersonByID(int personID) {
		Criteria criteria = getSession().createCriteria(Person.class).add(
				Restrictions.eq("personID", personID));
		return (List<Person>) criteria.list();
	}

	public void updatePerson(Person person) {
		update(person);
	}

	public boolean isPersonAvailable(String firstName, String middleName,
			String lastName) {
		Criteria criteria = getSession().createCriteria(Person.class)
				.setProjection(Property.forName("personID"))
				.add(Restrictions.eq("firstName", firstName))
				.add(Restrictions.eq("middleName", middleName))
				.add(Restrictions.eq("lastName", lastName));
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	public boolean isPersonDetailsChange(String firstName, String middleName,
			String lastName, int personID) {
		Criteria criteria = getSession().createCriteria(Person.class)
				.setProjection(Property.forName("personID"))
				.add(Restrictions.eq("firstName", firstName))
				.add(Restrictions.eq("middleName", middleName))
				.add(Restrictions.eq("lastName", lastName))
				.add(Restrictions.eq("personID", personID));
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

}
