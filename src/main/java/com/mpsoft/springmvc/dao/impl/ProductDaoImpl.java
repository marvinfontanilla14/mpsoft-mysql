package com.mpsoft.springmvc.dao.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mpsoft.springmvc.dao.AbstractDao;
import com.mpsoft.springmvc.dao.ProductDao;
import com.mpsoft.springmvc.model.Product;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDao implements ProductDao {

	public void saveProduct(Product product) {
		save(product);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> productList() {
		String HQL_QUERY = "select "
				+ "new map(c.ctgyID as ctgyID,c.ctgyName as ctgyName,p.prodID as prodID,p.prodCode as prodCode,"
				+ "p.prodName as prodName,p.prodCost as prodCost,"
				+ "p.sellingPrice as sellingPrice,p.orderPoint as orderPoint,"
				+ "p.dateCreated as dateCreated,p.timeCreated as timeCreated) "
				+ "from Product p join p.category c";
		return getSession().createQuery(HQL_QUERY).list();
	}

	public void deleteProduct(int prodID) {
		Criteria criteria = getSession().createCriteria(Product.class).add(
				Restrictions.eq("prodID", prodID));
		Product product = (Product) criteria.uniqueResult();
		delete(product);
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getProductByID(int prodID) {
		String HQL_QUERY = "select "
				+ "new map(c.ctgyID as ctgyID,c.ctgyName as ctgyName,p.prodID as prodID,p.prodCode as prodCode,"
				+ "p.prodName as prodName,p.prodCost as prodCost,"
				+ "p.sellingPrice as sellingPrice,p.orderPoint as orderPoint,"
				+ "p.dateCreated as dateCreated,p.timeCreated as timeCreated) "
				+ "from Product p join p.category c where p.prodID =:prodID";
		Query query = getSession().createQuery(HQL_QUERY);
		query.setParameter("prodID", prodID);
		return query.list();
	}

	public void updateProduct(Product product) {
		update(product);
	}

	public boolean isProductAvailable(int ctgyID, String prodCode,
			String prodName) {
		Criteria criteria = getSession().createCriteria(Product.class)
				.setFetchMode("category", FetchMode.JOIN)
				.setProjection(Property.forName("prodID"))
				.add(Restrictions.eq("category.ctgyID", ctgyID))
				.add(Restrictions.eq("prodCode", prodCode))
				.add(Restrictions.eq("prodName", prodName));
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	public boolean isProductDetailsChange(int ctgyID, String prodCode,
			String prodName, int prodID) {
		Criteria criteria = getSession().createCriteria(Product.class)
				.setFetchMode("category", FetchMode.JOIN)
				.setProjection(Property.forName("prodID"))
				.add(Restrictions.eq("category.ctgyID", ctgyID))
				.add(Restrictions.eq("prodCode", prodCode))
				.add(Restrictions.eq("prodName", prodName))
				.add(Restrictions.eq("prodID", prodID));
		if (criteria.uniqueResult() == null) {
			return true;
		}
		return false;
	}

	public int latestProductID() {
		Criteria criteria = getSession().createCriteria(Product.class)
				.setProjection(Property.forName("prodID"))
				.addOrder(Order.desc("prodID")).setMaxResults(1);
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

}
