package com.mpsoft.springmvc.dao;

import java.util.List;
import java.util.Map;

import com.mpsoft.springmvc.model.Product;

public interface ProductDao {

	void saveProduct(Product product);
	
	List<Map<String, Object>>productList();
	
	void deleteProduct(int prodID);
	
	List<Map<String, Object>> getProductByID(int prodID);
	
	void updateProduct(Product product);
	
	boolean isProductAvailable(int ctgyID,String prodCode, String prodName);
	
	boolean isProductDetailsChange(int ctgyID,String prodCode, String prodName,int prodID);
	
	int latestProductID();
}
