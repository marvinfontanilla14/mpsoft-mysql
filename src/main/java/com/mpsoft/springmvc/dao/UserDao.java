package com.mpsoft.springmvc.dao;

import java.util.List;

import com.mpsoft.springmvc.model.User;

public interface UserDao {

	void saveUser(User user);
	
	void updateUser(User user);

	List<User> getUserList();
	
	List<User> getUserByID(int userID);
	
	String isValidUser(String userName);
	
	String getUserRoleByUserName(String userName);
	
	boolean isUserAvailable(String userName);
	
	boolean isUserDetailsChange(String userName,int userID);
}
