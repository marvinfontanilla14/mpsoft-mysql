package com.mpsoft.springmvc.dao;

import java.util.List;
import java.util.Map;

import com.mpsoft.springmvc.model.Category;

public interface CategoryDao {

	void saveCategory(Category category);

	List<Map<String, Object>> categoryList();

	void deleteCategory(int ctgyID);

	List<Map<String, Object>> getCategoryByID(int ctgyID);
	
	List<Category> getCategoryBeanByID(int ctgyID);

	void updateCategory(Category category);

	boolean isCategoryAvailable(String ctgyName);

	boolean isCategoryDetailsChange(String ctgyName, int ctgyID);
	
	boolean canBeDeleted(int ctgyID);

}
