package com.mpsoft.springmvc.dao;

import java.util.List;

import com.mpsoft.springmvc.model.UserLevelSetting;

public interface UserLevelSettingDao {

	List<UserLevelSetting> userLevelList();

	List<UserLevelSetting> getUserLevelByRole(int userRole);

	void saveUserLevel(UserLevelSetting userLevel);

	void updateUserLevel(UserLevelSetting userLevel);

	boolean hasRoleSettings(int userRole);
}
