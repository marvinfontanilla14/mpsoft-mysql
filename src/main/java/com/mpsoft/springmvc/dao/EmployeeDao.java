package com.mpsoft.springmvc.dao;

import java.util.List;
import java.util.Map;

import com.mpsoft.springmvc.model.Employee;

public interface EmployeeDao {

	void saveEmployee(Employee employee);
	
	List<Map<String, Object>> employeeList();
	
	void deleteEmployee(int empID);
	
	List<Map<String, Object>> getEmployeeByID(int empID);
	
	List<Employee> getEmployeeBeanByID(int empID);
	
	void updateEmployee(Employee employee);
	
	boolean isEmployeeAvailable(String firstName,String middleName,String lastName,String jobPosition);
	
	boolean isEmployeeDetailsChange(String firstName,String middleName,String lastName,String jobPosition,int empID);
	
	int latestEmployeeID();
	
	boolean canBeDeleted(int empID);
}
