package com.mpsoft.springmvc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mpsoft.springmvc.model.Employee;
import com.mpsoft.springmvc.model.Project;
import com.mpsoft.springmvc.service.EmployeeService;
import com.mpsoft.springmvc.service.ProjectService;

@RestController
@RequestMapping("/many-to-many")
@PropertySource(value = { "classpath:application.properties" })
public class ManyToManyController {

	@Autowired
	ProjectService projectService;

	@Autowired
	EmployeeService employeeService;

	@Autowired
	private Environment environment;

	private static final Logger logger = Logger
			.getLogger(ManyToManyController.class);

	@RequestMapping(value = { "/project-list" }, method = RequestMethod.GET)
	public List<Map<String, Object>> projectList() {
		return projectService.projectList();
	}

	@RequestMapping(value = { "/save-project" })
	public String saveProject(@RequestBody Project project) {
		if (project.getProjID() == 0) {
			if (projectService.isProjectAvailable(project.getProjCode(),
					project.getProjCtgy(), project.getProjName())) {

				project.setDateCreated(new Date());
				project.setTimeCreated(new Date());
				projectService.saveProject(project);
				return "success";
			}
		} else {
			for (Map<String, Object> map : projectService
					.getProjectByID(project.getProjID())) {
				project.setDateCreated((Date) map.get("dateCreated"));
				project.setTimeCreated((Date) map.get("timeCreated"));
			}

			if (projectService.isProjectDetailsChange(project.getProjCode(),
					project.getProjCtgy(), project.getProjName(),
					project.getProjID())) {
				if (projectService.isProjectAvailable(project.getProjCode(),
						project.getProjCtgy(), project.getProjName())) {
					projectService.updateProject(project);
					return "success";
				}
			} else {
				projectService.updateProject(project);
				return "success";
			}
		}
		return "invalid";
	}

	@RequestMapping(value = { "/get-project-by-id/{projID}" }, method = RequestMethod.GET)
	public List<Map<String, Object>> getProjectByID(@PathVariable int projID) {
		return projectService.getProjectByID(projID);
	}

	@RequestMapping(value = { "/delete-project-by-id/{projID}" }, method = RequestMethod.GET)
	public String deleteProjectByID(@PathVariable int projID) {
		if (projectService.canBeDeleted(projID)) {
			projectService.deleteProject(projID);
			return "success";
		}
		return "invalid";
	}

	@RequestMapping(value = { "/latest-projid" }, method = RequestMethod.GET)
	public int getLatestProjID() {
		return projectService.latestProjectID();
	}

	@RequestMapping(value = { "/employee-list" }, method = RequestMethod.GET)
	public List<Map<String, Object>> employeeList() {
		return employeeService.employeeList();
	}

	@RequestMapping(value = { "/save-employee" })
	public String saveEmployee(@RequestBody Employee employee) {
		if (employee.getEmpID() == 0) {
			if (employeeService.isEmployeeAvailable(employee.getFirstName(),
					employee.getMiddleName(), employee.getLastName(),
					employee.getJobPosition())) {
				employee.setDateCreated(new Date());
				employee.setTimeCreated(new Date());
				employeeService.saveEmployee(employee);
				return "success";
			}
		} else {
			for (Map<String, Object> map : employeeService
					.getEmployeeByID(employee.getEmpID())) {
				employee.setDateCreated((Date) map.get("dateCreated"));
				employee.setTimeCreated((Date) map.get("dateCreated"));
			}

			if (employeeService.isEmployeeDetailsChange(
					employee.getFirstName(), employee.getMiddleName(),
					employee.getLastName(), employee.getJobPosition(),
					employee.getEmpID())) {
				if (employeeService.isEmployeeAvailable(
						employee.getFirstName(), employee.getMiddleName(),
						employee.getLastName(), employee.getJobPosition())) {
					employeeService.updateEmployee(employee);
					return "success";
				}
			} else {
				employeeService.updateEmployee(employee);
				return "success";
			}
		}
		return "invalid";
	}

	@RequestMapping(value = { "/latest-empid" }, method = RequestMethod.GET)
	public int getLatestEmpID() {
		return employeeService.latestEmployeeID();
	}

	@RequestMapping(value = { "/get-employee-by-id/{empjID}" }, method = RequestMethod.GET)
	public List<Map<String, Object>> geEmployeeByID(@PathVariable int empjID) {
		return employeeService.getEmployeeByID(empjID);
	}

	@RequestMapping(value = { "/delete-employee-by-id/{empID}" }, method = RequestMethod.GET)
	public String deleteEmployeeByID(@PathVariable int empID) {
		if (employeeService.canBeDeleted(empID)) {
			employeeService.deleteEmployee(empID);
			return "success";
		}
		return "invalid";
	}

	@RequestMapping(value = { "/project-employee-list" }, method = RequestMethod.GET)
	public List<Map<String, Object>> projectEmployeeList() {
		return projectService.projectEmployeeList();
	}

	@RequestMapping(value = { "/link-project-employee/{projID}/{empID}" }, method = RequestMethod.GET)
	public String linkProjectEmployee(@PathVariable int projID,
			@PathVariable int empID) {
		Project project = new Project();
		Employee employee = new Employee();
		for (Project p : projectService.getProjectBeanByID(projID)) {
			project = p;
		}
		for (Employee e : employeeService.getEmployeeBeanByID(empID)) {
			employee = e;
		}
		List<Employee> employeeList = new ArrayList<Employee>();
		employeeList.add(employee);
		project.setEmployee(employeeList);
		projectService.saveProject(project);
		return "success";
	}

}
