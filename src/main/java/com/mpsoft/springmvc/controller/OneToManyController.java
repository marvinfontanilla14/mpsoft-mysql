package com.mpsoft.springmvc.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mpsoft.springmvc.model.Category;
import com.mpsoft.springmvc.model.Product;
import com.mpsoft.springmvc.service.CategoryService;
import com.mpsoft.springmvc.service.ProductService;

@RestController
@RequestMapping("/one-to-many")
@PropertySource(value = { "classpath:application.properties" })
public class OneToManyController {

	@Autowired
	CategoryService categoryService;

	@Autowired
	ProductService productService;

	@Autowired
	private Environment environment;

	private static final Logger logger = Logger
			.getLogger(OneToManyController.class);

	@RequestMapping(value = { "/category-list" }, method = RequestMethod.GET)
	public List<Map<String, Object>> categoryList() {
		return categoryService.categoryList();
	}

	@RequestMapping(value = { "/save-category" })
	public String saveCategory(@RequestBody Category category) {
		if (category.getCtgyID() == 0) {
			if (categoryService.isCategoryAvailable(category.getCtgyName())) {
				category.setDateCreated(new Date());
				category.setTimeCreated(new Date());
				categoryService.saveCategory(category);
				return "success";
			}
		} else {
			for (Map<String, Object> map : categoryService
					.getCategoryByID(category.getCtgyID())) {
				category.setDateCreated((Date) map.get("dateCreated"));
				category.setTimeCreated((Date) map.get("timeCreated"));
			}

			if (categoryService.isCategoryDetailsChange(category.getCtgyName(),
					category.getCtgyID())) {
				if (categoryService.isCategoryAvailable(category.getCtgyName())) {
					categoryService.updateCategory(category);
					return "success";
				}
			} else {
				categoryService.updateCategory(category);
				return "success";
			}
		}

		return "invalid";
	}

	@RequestMapping(value = { "/get-category-by-id/{ctgyID}" }, method = RequestMethod.GET)
	public List<Map<String, Object>> getCategoryByID(@PathVariable int ctgyID) {
		return categoryService.getCategoryByID(ctgyID);
	}

	@RequestMapping(value = { "/delete-category-by-id/{ctgyID}" }, method = RequestMethod.GET)
	public String deleteCategoryByID(@PathVariable int ctgyID) {
		if (categoryService.canBeDeleted(ctgyID)) {
			categoryService.deleteCategory(ctgyID);
			return "success";
		}
		return "invalid";
	}

	@RequestMapping(value = { "/product-list" }, method = RequestMethod.GET)
	public List<Map<String, Object>> productList() {
		return productService.productList();
	}

	@RequestMapping(value = { "/save-product" })
	public String saveProduct(@RequestBody Product product) {
		Category category = product.getCategory();

		for (Category c : categoryService.getCategoryBeanByID(category
				.getCtgyID())) {
			category = c;
		}

		if (product.getProdID() == 0) {
			if (productService.isProductAvailable(category.getCtgyID(),
					product.getProdCode(), product.getProdName())) {
				product.setCategory(category);
				product.setDateCreated(new Date());
				product.setTimeCreated(new Date());
				productService.saveProduct(product);
				return "success";
			}
		} else {
			for (Map<String, Object> map : productService
					.getProductByID(product.getProdID())) {
				product.setDateCreated((Date) map.get("dateCreated"));
				product.setTimeCreated((Date) map.get("timeCreated"));
			}

			if (productService.isProductDetailsChange(category.getCtgyID(),
					product.getProdCode(), product.getProdName(),
					product.getProdID())) {
				if (productService.isProductAvailable(category.getCtgyID(),
						product.getProdCode(), product.getProdName())) {
					product.setCategory(category);
					productService.updateProduct(product);
					return "success";
				}
			} else {
				product.setCategory(category);
				productService.updateProduct(product);
				return "success";
			}
		}
		return "invalid";
	}

	@RequestMapping(value = { "/get-product-by-id/{prodID}" }, method = RequestMethod.GET)
	public List<Map<String, Object>> getProductByID(@PathVariable int prodID) {
		return productService.getProductByID(prodID);
	}

	@RequestMapping(value = { "/delete-product-by-id/{prodID}" }, method = RequestMethod.GET)
	public String deleteProductByID(@PathVariable int prodID) {
		productService.deleteProduct(prodID);
		return "success";
	}

	@RequestMapping(value = { "/latest-prodid" }, method = RequestMethod.GET)
	public int getLatestProdID() {
		return productService.latestProductID();
	}

}
