package com.mpsoft.springmvc.controller;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mpsoft.springmvc.model.User;
import com.mpsoft.springmvc.service.UserService;

@RestController
@RequestMapping(value = { "/user-account" })
@PropertySource(value = { "classpath:application.properties" })
public class UserAccountController {

	@Autowired
	UserService userService;

	@Autowired
	private Environment environment;

	private static final Logger logger = Logger
			.getLogger(UserAccountController.class);

	@RequestMapping(value = { "/user-list" }, method = RequestMethod.GET)
	private List<User> userList() {
		return userService.getUserList();
	}

	@RequestMapping(value = { "/get-user-by-id/{userID}" }, method = RequestMethod.GET)
	private List<User> getUserByID(@PathVariable int userID) {
		return userService.getUserByID(userID);
	}

	@RequestMapping(value = { "/save-user" })
	private String saveUser(@RequestBody User user) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		user.setUserPassword(passwordEncoder.encode(user.getUserPassword()));

		if (user.getUserID() == 0) {
			if (userService.isUserAvailable(user.getUserName())) {
				user.setDateCreated(new Date());
				user.setTimeCreated(new Date());
				userService.saveUser(user);
				return "success";
			}
		} else {
			for (User u : userService.getUserByID(user.getUserID())) {
				user.setDateCreated(u.getDateCreated());
				user.setTimeCreated(u.getTimeCreated());
			}
			if (userService.isUserDetailsChange(user.getUserName(),
					user.getUserID())) {
				if (userService.isUserAvailable(user.getUserName())) {
					userService.updateUser(user);
					return "success";
				}
			} else {
				userService.updateUser(user);
				return "success";
			}

		}
		return "invalid";
	}

	@RequestMapping(value = { "/get-user-role-by-username/{userName}" }, method = RequestMethod.GET)
	private String getUserRoleByUserName(@PathVariable String userName) {
		return userService.getUserRoleByUserName(userName);
	}
}
