package com.mpsoft.springmvc.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mpsoft.springmvc.model.Game;
import com.mpsoft.springmvc.model.Gamer;
import com.mpsoft.springmvc.service.GamerService;

@RestController
@RequestMapping("/many-to-many-simple")
@PropertySource(value = { "classpath:application.properties" })
public class ManyToManySimplifiedController {

	@Autowired
	GamerService gamerService;

	@Autowired
	private Environment environment;

	private static final Logger logger = Logger
			.getLogger(ManyToManyController.class);

	@RequestMapping(value = { "/gamer-list" }, method = RequestMethod.GET)
	public List<Map<String, Object>> gamerList() {
		return gamerService.gamerList();
	}

	@RequestMapping(value = { "/save-gamer/{gamerName}/{strGames}" }, method = RequestMethod.GET)
	public String saveGamer(@PathVariable String gamerName,
			@PathVariable String strGames) {
		Gamer gamer = new Gamer();
		gamer.setGamerName(gamerName);
		Collection<Game> gameList = new ArrayList<Game>();

		String gameArray[] = strGames.split(",");

		for (int i = 0; i < gameArray.length; i++) {
			Game game = new Game();
			game.setGameName(gameArray[i]);
			gameList.add(game);
		}

		gamer.setGameList(gameList);

		gamerService.saveGamer(gamer);

		return "success";
	}

	@RequestMapping(value = { "/delete-gamer-by-id/{gamerID}" }, method = RequestMethod.GET)
	public String deleteGamerByID(@PathVariable int gamerID) {
		gamerService.deleteGamerByID(gamerID);
		return "success";
	}
}
