package com.mpsoft.springmvc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.mpsoft.springmvc.service.CategoryService;

@RestController
@RequestMapping(value = "/report-view")
@PropertySource(value = { "classpath:application.properties" })
public class ReportGeneratorController {

	@Autowired
	CategoryService categoryService;

	@Autowired
	private Environment environment;

	private static final Logger logger = Logger
			.getLogger(ManyToManyController.class);

	@RequestMapping(method = RequestMethod.GET, value = { "/category-report" })
	public ModelAndView generatePdfReport(ModelAndView modelAndView) {

		logger.debug("--------------generate PDF report----------");

		Map<String, Object> parameterMap = new HashMap<String, Object>();

		List<Map<String, Object>> category = categoryService.categoryList();

		JRDataSource JRdataSource = new JRBeanCollectionDataSource(category);

		parameterMap.put("datasource", JRdataSource);

		modelAndView = new ModelAndView("categoryListReport", parameterMap);

		return modelAndView;
	}
}
