package com.mpsoft.springmvc.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.mpsoft.springmvc.model.User;
import com.mpsoft.springmvc.service.UserService;

@RestController
@RequestMapping("/")
@SessionAttributes("SYSTEMUSER")
@PropertySource(value = { "classpath:application.properties" })
public class MainController {

	@Autowired
	UserService userService;

	@Autowired
	private Environment environment;

	// -- Log4j : used for logging (Info,Error,Warning etc.)
	private static final Logger logger = Logger.getLogger(MainController.class);

	/*
	 * Show main page if user session has been set otherwise, return/show login
	 * page.
	 */
	@RequestMapping(value = { "/", "main" }, method = RequestMethod.GET)
	public ModelAndView defaultView(HttpSession session) {
		if (session.getAttribute(environment
				.getRequiredProperty("session.variable")) != null) {
			return new ModelAndView("index");
		}
		return new ModelAndView("redirect:user-authentication");
	}

	@RequestMapping(value = { "/dashboard" }, method = RequestMethod.GET)
	public ModelAndView dashboard(HttpSession session) {
		if (session.getAttribute(environment
				.getRequiredProperty("session.variable")) != null) {
			return new ModelAndView("dashboard");
		}
		return new ModelAndView("redirect:user-authentication");
	}

	// -- Login page
	@RequestMapping(value = { "/user-authentication" }, method = RequestMethod.GET)
	public ModelAndView userLoginView(HttpSession session) {
		ModelAndView mav = new ModelAndView("login-view");
		if (session.getAttribute(environment
				.getRequiredProperty("session.variable")) != null) {
			// return new ModelAndView("main-view");
			return new ModelAndView("index");
		}
		return mav;
	}

	// -- Used to validate user credentials
	@RequestMapping(value = { "/user-authentication/validate-user/" }, method = RequestMethod.POST)
	public String validateUserAccount(@RequestBody User user) {

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		// -- Used to encode password
		// passwordEncoder.encode(user.getUserPassword());

		String encodedPass = userService.isValidUser(user.getUserName());
		if (encodedPass.length() > 0) {
			if (passwordEncoder.matches(user.getUserPassword(), encodedPass)) {
				return "success";
			}
			return "invalid";
		}
		return "invalid";
	}

	// -- If user credentials is valid, user session will be set
	@RequestMapping(value = { "/user-authentication/set-session/" })
	public ModelAndView setSession(@RequestBody User user) {
		ModelAndView mav = new ModelAndView("main-view");
		mav.addObject(environment.getRequiredProperty("session.variable"),
				user.getUserName());
		return mav;
	}

	// -- User Logout
	@RequestMapping(value = { "/user-logout" }, method = RequestMethod.GET)
	public ModelAndView userLogout(HttpSession session, Model model) {
		session.invalidate();
		if (model.containsAttribute(environment
				.getRequiredProperty("session.variable"))) {
			model.asMap().remove(
					environment.getRequiredProperty("session.variable"));
		}
		return new ModelAndView("redirect:user-authentication");
	}

	// -- MENU

	@RequestMapping(value = { "/one-to-one" }, method = RequestMethod.GET)
	public ModelAndView oneToOne(HttpSession session) {
		if (session.getAttribute(environment
				.getRequiredProperty("session.variable")) != null) {
			return new ModelAndView("crudoperations/one-to-one");
		}
		return new ModelAndView("redirect:user-authentication");
	}

	@RequestMapping(value = { "/one-to-many" }, method = RequestMethod.GET)
	public ModelAndView oneToMany(HttpSession session) {
		if (session.getAttribute(environment
				.getRequiredProperty("session.variable")) != null) {
			return new ModelAndView("crudoperations/one-to-many");
		}
		return new ModelAndView("redirect:user-authentication");
	}

	@RequestMapping(value = { "/many-to-many" }, method = RequestMethod.GET)
	public ModelAndView manyToMany(HttpSession session) {
		if (session.getAttribute(environment
				.getRequiredProperty("session.variable")) != null) {
			return new ModelAndView("crudoperations/many-to-many");
		}
		return new ModelAndView("redirect:user-authentication");
	}

	@RequestMapping(value = { "/many-to-many-simple" }, method = RequestMethod.GET)
	public ModelAndView manyToManySimple(HttpSession session) {
		if (session.getAttribute(environment
				.getRequiredProperty("session.variable")) != null) {
			return new ModelAndView("crudoperations/many-to-many-simple");
		}
		return new ModelAndView("redirect:user-authentication");
	}

	@RequestMapping(value = { "/user-account" }, method = RequestMethod.GET)
	public ModelAndView userAccountView(HttpSession session) {
		if (session.getAttribute(environment
				.getRequiredProperty("session.variable")) != null) {
			return new ModelAndView("utilities/user-account");
		}
		return new ModelAndView("redirect:user-authentication");
	}

	@RequestMapping(value = { "/user-level-setting" }, method = RequestMethod.GET)
	public ModelAndView userLevelView(HttpSession session) {
		if (session.getAttribute(environment
				.getRequiredProperty("session.variable")) != null) {
			return new ModelAndView("utilities/user-level-setting");
		}
		return new ModelAndView("redirect:user-authentication");
	}

	@RequestMapping(value = { "/backup-restore" }, method = RequestMethod.GET)
	public ModelAndView backupRestoreView(HttpSession session) {
		if (session.getAttribute(environment
				.getRequiredProperty("session.variable")) != null) {
			return new ModelAndView("utilities/backup-restore");
		}
		return new ModelAndView("redirect:user-authentication");
	}
}
