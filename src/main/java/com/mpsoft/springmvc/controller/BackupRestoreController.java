package com.mpsoft.springmvc.controller;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mpsoft.springmvc.utilities.UIFunctions;

@RestController
@RequestMapping("/backup-restore")
@PropertySource(value = { "classpath:application.properties" })
public class BackupRestoreController {

	@Autowired
	private Environment environment;

	private static final Logger logger = Logger
			.getLogger(BackupRestoreController.class);

	private UIFunctions ui = new UIFunctions();

	@RequestMapping(value = { "/backup" })
	private String testBackup() {

		String folderPath = environment.getRequiredProperty("br.folderPath")
				+ environment.getRequiredProperty("br.folderName") + "\\";
		File f1 = new File(folderPath);
		f1.mkdir();

		String fileName = ui.getDateTime("MMddyyyyhhmmss") + "-"
				+ environment.getRequiredProperty("br.addText") + ".sql";
		String executeCmd = environment.getRequiredProperty("br.mysqlDumpPath")
				+ " -u" + environment.getRequiredProperty("jdbc.username")
				+ " -p" + environment.getRequiredProperty("jdbc.password")
				+ " --database " + environment.getRequiredProperty("br.dbName")
				+ " -r" + folderPath + fileName;
		try {
			Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
			int processComplete = runtimeProcess.waitFor();
			if (processComplete == 0) {
				Thread.sleep(5000);
				return "success";
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return "error";
	}

	@RequestMapping(value = { "/backup-file" })
	private String[] getDBBackupFiles() {
		String folderPath = environment.getRequiredProperty("br.folderPath")
				+ environment.getRequiredProperty("br.folderName") + "\\";
		File dir = new File(folderPath);
		String[] children = dir.list();
		return children;
	}

	@RequestMapping(value = { "/restore/{sqlFile}" })
	private String testRestore(@PathVariable String sqlFile) {
		String folderPath = environment.getRequiredProperty("br.folderPath")
				+ environment.getRequiredProperty("br.folderName") + "\\";
		
		String[] executeCmd = new String[] {
				environment.getRequiredProperty("br.mysqlRestore"),
				environment.getRequiredProperty("br.dbName"),
				"-u" + environment.getRequiredProperty("jdbc.username"),
				"-p" + environment.getRequiredProperty("jdbc.password"), "-e",
				" source " + folderPath + sqlFile + ".sql" };
		
		Process runtimeProcess;
		try {
			runtimeProcess = Runtime.getRuntime().exec(executeCmd);
			int processComplete = runtimeProcess.waitFor();
			if (processComplete == 0) {
				return "success";
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return "invalid";
	}

}
