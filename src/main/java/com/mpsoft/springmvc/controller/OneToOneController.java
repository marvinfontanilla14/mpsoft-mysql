package com.mpsoft.springmvc.controller;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mpsoft.springmvc.model.Passport;
import com.mpsoft.springmvc.model.Person;
import com.mpsoft.springmvc.service.PassportService;
import com.mpsoft.springmvc.service.PersonService;

@RestController
@RequestMapping("/one-to-one")
@PropertySource(value = { "classpath:application.properties" })
public class OneToOneController {

	@Autowired
	PersonService personService;

	@Autowired
	PassportService passportService;

	@Autowired
	private Environment environment;

	private static final Logger logger = Logger
			.getLogger(OneToOneController.class);

	@RequestMapping(value = { "/person-list" }, method = RequestMethod.GET)
	public List<Person> personList() {
		// -- Return list of person
		return personService.personList();
	}

	@RequestMapping(value = { "/latest-passid" }, method = RequestMethod.GET)
	public int getLatestPassID() {
		return passportService.latestPassportID();
	}

	@RequestMapping(value = { "/save-personpassport" })
	public String savePersonPassport(@RequestBody Person person) {
		Passport passport = person.getPassport();
		if (person.getPersonID() == 0) {
			if (personService.isPersonAvailable(person.getFirstName(),
					person.getMiddleName(), person.getLastName())) {

				person.setDateRegist(new Date());
				person.setTimeRegist(new Date());
				person.setPassport(passport);
				passportService.savePassport(passport);
				personService.savePerson(person);

				return "success";
			}
		} else {
			for (Person p : personService.getPersonByID(person.getPersonID())) {
				person.setDateRegist(p.getDateRegist());
				person.setTimeRegist(p.getTimeRegist());
			}

			if (personService.isPersonDetailsChange(person.getFirstName(),
					person.getMiddleName(), person.getLastName(),
					person.getPersonID())) {
				if (personService.isPersonAvailable(person.getFirstName(),
						person.getMiddleName(), person.getLastName())) {

					person.setPassport(passport);
					passportService.updatePassport(passport);
					personService.updatePerson(person);

					return "success";
				}
			} else {
				person.setPassport(passport);
				passportService.updatePassport(passport);
				personService.updatePerson(person);
				return "success";
			}
		}

		return "invalid";
	}

	@RequestMapping(value = { "/get-person-by-id/{personID}" }, method = RequestMethod.GET)
	public List<Person> getPersonByID(@PathVariable int personID) {
		return personService.getPersonByID(personID);
	}

	@RequestMapping(value = { "/delete-person-by-id/{personID}" }, method = RequestMethod.GET)
	public String deletePersonByID(@PathVariable int personID) {
		List<Person> personList = personService.getPersonByID(personID);

		for (Person person : personList) {
			Passport passport = person.getPassport();
			personService.deletePerson(person);
			passportService.deletePassport(passport);
		}

		return "success";
	}

}
