package com.mpsoft.springmvc.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mpsoft.springmvc.model.UserLevelSetting;
import com.mpsoft.springmvc.service.UserLevelSettingService;

@RestController
@RequestMapping(value = { "/user-level-setting" })
@PropertySource(value = { "classpath:application.properties" })
public class UserLevelSettingController {

	@Autowired
	UserLevelSettingService userLevelSettingService;

	@Autowired
	private Environment environment;

	private static final Logger logger = Logger
			.getLogger(UserLevelSettingController.class);

	@RequestMapping(value = { "/user-level-list" }, method = RequestMethod.GET)
	private List<UserLevelSetting> getUserLevelList() {
		return userLevelSettingService.userLevelList();
	}

	@RequestMapping(value = { "/get-user-level-by-role/{userRole}" }, method = RequestMethod.GET)
	private List<UserLevelSetting> getUserLevelByRole(@PathVariable int userRole) {
		return userLevelSettingService.getUserLevelByRole(userRole);
	}

	@RequestMapping(value = { "/save-user-level" })
	private String saveUserLevel(@RequestBody UserLevelSetting userLevel) {
		if (!userLevelSettingService.hasRoleSettings(userLevel.getUserRole())) {
			userLevelSettingService.saveUserLevel(userLevel);
		}
		for (UserLevelSetting ul : userLevelSettingService
				.getUserLevelByRole(userLevel.getUserRole())) {
			userLevel.setUserLevelID(ul.getUserLevelID());
		}
		userLevelSettingService.updateUserLevel(userLevel);
		return "success";
	}
}
