function selectSettings() {
	$('#restore').select2();
}

function formValidations() {
	$('#defaultForm')
			.bootstrapValidator(
					{
						fields : {
							restore : {
								validators : {
									notEmpty : {
										message : 'The database is required and can\'t be empty'
									}
								}
							},
						}
					}).on('submit', function(event) {
				event.preventDefault();
				if ($('#restore').val().length === 0) {
					return;
				}
				doRestore();
			});
}

function populateUserRole() {
	for (var i = 0; i <= $userRole; i++) {
		$('#userRole').append(
				'<option value="' + i + '">Level ' + i + '</option>');
	}
}

function loadBackupFile() {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'backup-restore/backup-file',
		async : true,
		success : function(result) {
			for (var i = 0; i < result.length; i++) {
				$('#restore').append(
						'<option value="' + result[i] + '">' + result[i]
								+ '</option>');
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});

}

function doBackup() {
	if (confirm("Backup database?")) {
		$('#btnbackup').html('');
		$('#btnbackup').append(
				'<img src="assets/img/ajax-loader.gif"/> Processing.. ');
		$.ajax({
			type : 'GET',
			url : $urlPrefix + 'backup-restore/backup',
			async : true,
			success : function(result) {
				if (result === 'success') {
					alert("Database has been successfully backup!");
					location.reload();
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.status + " " + jqXHR.responseText);
			}
		});
	}
}

function doRestore() {
	if (confirm("Warning: Current data will be replaced/removed with the selected database. Are you sure you want to restore selected database?")) {
		$('#btnrestore').html('');
		$('#btnrestore').append(
				'<img src="assets/img/ajax-loader.gif"/> Processing.. ');
		$.ajax({
			type : 'GET',
			url : $urlPrefix + 'backup-restore/restore/' + $('#restore').val(),
			async : true,
			success : function(result) {
				if (result === 'success') {
					alert("Database has been successfully restored!");
					location.reload();
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.status + " " + jqXHR.responseText);
			}
		});
	}
}