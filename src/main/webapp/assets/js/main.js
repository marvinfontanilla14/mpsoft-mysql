$(document).ready(function() {
	disableLinks();
});

function disableLinks() {
	$role = "";
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'user-account/get-user-role-by-username/'
				+ $systemUser,
		async : false,
		success : function(result) {
			$role = result;
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});

	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'user-level-setting/get-user-level-by-role/'
				+ $role,
		dataType : 'json',
		async : true,
		success : function(result) {
			if (result.toString() === '') {
				$("#idResources").last().addClass(setActive(false));
				$("#idCrud").last().addClass(setActive(false));
				$("#idJasper").last().addClass(setActive(false));
				$("#idUtilities").last().addClass(setActive(false));
			}
			$.each(result,
					function(index, element) {
						$("#idResources").last().addClass(
								setActive(element.activeResources));
						$("#idCrud").last().addClass(
								setActive(element.activeCrud));
						$("#idJasper").last().addClass(
								setActive(element.activeJasper));
						$("#idUtilities").last().addClass(
								setActive(element.activeUtilities));
					});
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});
}

function setActive($bol) {
	if ($bol === true) {
		return "";
	}
	return "not-active";
}