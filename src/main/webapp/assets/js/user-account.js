function selectSettings() {
	$('#userRole').select2();
}

function MakeSelect2() {
	$('select').select2();
	$('.dataTables_filter').each(function() {
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}

function loadTableSettings() {
	tableSettings();
	LoadSelect2Script(MakeSelect2);
}

function tableSettings() {
	var asInitVals = [];
	var oTable = $('#datatable')
			.dataTable(
					{
						"aaSorting" : [ [ 0, "asc" ] ],
						"sDom" : "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
						"sPaginationType" : "bootstrap",
						"oLanguage" : {
							"sSearch" : "",
							"sLengthMenu" : '_MENU_'
						},
						"aoColumnDefs" : [ {
							'sWidth' : '25%',
							'aTargets' : [ 0 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 1 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 2 ]
						}, {
							'sWidth' : '5%',
							'aTargets' : [ 3 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 1 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 2 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 3 ]
						} ],
						bAutoWidth : false,
					});
	var header_inputs = $("#datatable thead input");
	header_inputs.on('keyup', function() {
		/* Filter on the column (the index) of this element */
		oTable.fnFilter(this.value, header_inputs.index(this));
	}).on('focus', function() {
		if (this.className == "search_init") {
			this.className = "";
			this.value = "";
		}
	}).on('blur', function(i) {
		if (this.value == "") {
			this.className = "search_init";
			this.value = asInitVals[header_inputs.index(this)];
		}
	});
	header_inputs.each(function(i) {
		asInitVals[i] = this.value;
	});
}

function loadTableData() {
	$
			.ajax({
				type : 'GET',
				url : $urlPrefix + 'user-account/user-list',
				dataType : 'json',
				async : true,
				success : function(result) {
					$
							.each(
									result,
									function(index, element) {

										$('#table_data')
												.append(
														'<tr><td>'
																+ element.userName
																+ '</td><td>'
																+ 'Level '
																+ element.userRole
																+ '</td><td>'
																+ formatStatus(element.status)
																+ '</td>'
																+ '<td>'
																+ '<button id="up'
																+ element.userID
																+ '" type="button" class="btn btn-primary btn-lg" style="width: 60px;margin-right:5px" onclick="editSelectedDetails('
																+ element.userID
																+ ')">Update</button>'
																+ '</td></tr>');
									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + " " + jqXHR.responseText);
				}
			});
}

function formatStatus($status) {
	if ($status === true) {
		return "Active";
	} else {
		return "Inactive";
	}
}

function formValidations() {
	$('#defaultForm')
			.bootstrapValidator(
					{
						fields : {
							userName : {
								message : 'The username is not valid',
								validators : {
									notEmpty : {
										message : 'The username is required and can\'t be empty'
									},
									stringLength : {
										min : 3,
										max : 25,
										message : 'The username must be more than 3 and less than 25 characters long'
									}
								}
							},
							userPassword : {
								validators : {
									notEmpty : {
										message : 'The password is required and can\'t be empty'
									},
									stringLength : {
										min : 3,
										max : 25,
										message : 'The password must be more than 3 and less than 25 characters long'
									},
									identical : {
										field : 'confirmPassword',
										message : 'The password and its confirm are not the same'
									}
								}
							},
							cuserPassword : {
								validators : {
									notEmpty : {
										message : 'The confirm password is required and can\'t be empty'
									},
									stringLength : {
										min : 3,
										max : 25,
										message : 'The confirm password must be more than 3 and less than 25 characters long'
									},
									identical : {
										field : 'userPassword',
										message : 'The password and its confirm are not the same'
									}
								}
							},
							userRole : {
								validators : {
									notEmpty : {
										message : 'The user role is required and can\'t be empty'
									}
								}
							},
						}
					}).on(
					'submit',
					function(event) {
						event.preventDefault();
						if ($('#userName').val().length < 3
								|| $('#userPassword').val().length < 3
								|| $('#cuserPassword').val().length < 3
								|| $('#userRole').val().length === 0) {
							return;
						}
						saveUser();
					});
}

function saveUser() {
	$jsonArray = {
		"userID" : $('#userID').val(),
		"userName" : $('#userName').val(),
		"userPassword" : $('#userPassword').val(),
		"userRole" : $('#userRole').val(),
		"status" : $("#status").is(":checked")
	};

	$.ajax({
		type : 'POST',
		url : $urlPrefix + 'user-account/save-user',
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify($jsonArray),
		async : true,
		success : function(result) {
			if (result === "success") {
				alert("Data has been successfully saved!");
				location.reload();
			} else {
				alert("Unable to save. User name already in used!");
				$("#userName").focus();
				$("html, body").animate({
					scrollTop : 0
				}, "slow");
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
		}
	});
}

function populateUserRole() {
	for (var i = 0; i <= $userRole; i++) {
		$('#userRole').append(
				'<option value="' + i + '">Level ' + i + '</option>');
	}
}

function editSelectedDetails($id) {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'user-account/get-user-by-id/' + $id,
		async : true,
		dataType : 'json',
		success : function(result) {
			$.each(result, function(index, element) {
				$('#userID').val(element.userID);
				$('#userName').val(element.userName);
				$('#userRole').val(element.userRole);
				LoadSelect2Script(selectSettings);
				$("#status").prop('checked', element.status);
				$("#status").prop('disabled', false);

				$("html, body").animate({
					scrollTop : 0
				}, "slow");

				$('#userName').focus();
			});
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});

}

function deleteSelectedData($id) {
	if (confirm("delete selected Data?")) {
		$.ajax({
			type : 'GET',
			url : $urlPrefix + 'one-to-one/delete-person-by-id/' + $id,
			async : true,
			success : function(result) {
				if (result == "success") {
					alert("Selected data has been deleted!");
					location.reload();
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.status + " " + jqXHR.responseText);
			}
		});
	}

}