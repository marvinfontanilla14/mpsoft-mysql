function selectSettings() {
	$('#userRole').select2();
}

function formValidations() {
	$('#defaultForm')
			.bootstrapValidator(
					{
						fields : {
							userRole : {
								validators : {
									notEmpty : {
										message : 'The user role is required and can\'t be empty'
									}
								}
							},
						}
					}).on('submit', function(event) {
				event.preventDefault();
				if ($('#userRole').val().length === 0) {
					return;
				}
				saveUserLevelSetting();
			});
}

function saveUserLevelSetting() {
	$jsonArray = {
		"userRole" : $('#userRole').val(),
		"activeResources" : $("#isActiveResources").is(":checked"),
		"activeCrud" : $("#isActiveCrud").is(":checked"),
		"activeJasper" : $("#isActiveJasper").is(":checked"),
		"activeUtilities" : $("#isActiveUtilities").is(":checked")
	};

	$.ajax({
		type : 'POST',
		url : $urlPrefix + 'user-level-setting/save-user-level',
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify($jsonArray),
		async : true,
		success : function(result) {
			if (result === "success") {
				alert("Data has been successfully saved!");
				location.reload();
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
		}
	});
}

function populateUserRole() {
	for (var i = 0; i <= $userRole; i++) {
		$('#userRole').append(
				'<option value="' + i + '">Level ' + i + '</option>');
	}
}

var $userLevelList;
function selectedRole() {
	$('#userRole').on(
			'change',
			function() {
				$("#isActiveResources").prop('checked', false);
				$("#isActiveCrud").prop('checked', false);
				$("#isActiveJasper").prop('checked', false);
				$("#isActiveUtilities").prop('checked', false);
				$.each($userLevelList, function(index, element) {
					if (element.userRole.toString() === $('#userRole').val()) {
						$("#isActiveResources").prop('checked',
								element.activeResources);
						$("#isActiveCrud").prop('checked', element.activeCrud);
						$("#isActiveJasper").prop('checked',
								element.activeJasper);
						$("#isActiveUtilities").prop('checked',
								element.activeUtilities);
					}
				});
			});
}

function loadUserLevelList() {

	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'user-level-setting/user-level-list',
		dataType : 'json',
		async : true,
		success : function(result) {
			$userLevelList = result;
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});

}