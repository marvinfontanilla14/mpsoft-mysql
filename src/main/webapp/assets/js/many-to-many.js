function TimePicker() {
	$('#dateStart').datepicker({});
	$('#dateAccmphld').datepicker({});
	$('#dateHired').datepicker({});
}

function cancelTrans($buttonId) {
	$("#" + $buttonId).click(function() {
		if (confirm("Cancel Transaction?")) {
			location.reload();
		}
	});
}
function selectSettings() {
	$('#projectList').select2();
	$('#employeeList').select2();
}

function MakeSelect2() {
	$('select').select2();
	$('.dataTables_filter').each(function() {
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}

function loadTableSettings() {
	projectTableSettings();
	employeeTableSettings();
	projEmpTableSettings();
	LoadSelect2Script(MakeSelect2);
}

function projectTableSettings() {
	var asInitVals = [];
	var oTable = $('#datatableProject')
			.dataTable(
					{
						"aaSorting" : [ [ 0, "asc" ] ],
						"sDom" : "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
						"sPaginationType" : "bootstrap",
						"oLanguage" : {
							"sSearch" : "",
							"sLengthMenu" : '_MENU_'
						},
						"aoColumnDefs" : [ {
							'sWidth' : '17%',
							'aTargets' : [ 0 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 1 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 2 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 3 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 4 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 5 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 3 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 4 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 5 ]
						} ],
						bAutoWidth : false,
					});
	var header_inputs = $("#datatableProject thead input");
	header_inputs.on('keyup', function() {
		/* Filter on the column (the index) of this element */
		oTable.fnFilter(this.value, header_inputs.index(this));
	}).on('focus', function() {
		if (this.className == "search_init") {
			this.className = "";
			this.value = "";
		}
	}).on('blur', function(i) {
		if (this.value == "") {
			this.className = "search_init";
			this.value = asInitVals[header_inputs.index(this)];
		}
	});
	header_inputs.each(function(i) {
		asInitVals[i] = this.value;
	});
}

function employeeTableSettings() {
	var asInitVals = [];
	var oTable = $('#datatableEmployee')
			.dataTable(
					{
						"aaSorting" : [ [ 0, "asc" ] ],
						"sDom" : "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
						"sPaginationType" : "bootstrap",
						"oLanguage" : {
							"sSearch" : "",
							"sLengthMenu" : '_MENU_'
						},
						"aoColumnDefs" : [ {
							'sWidth' : '20%',
							'aTargets' : [ 0 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 1 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 2 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 3 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 4 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 3 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 4 ]
						} ],
						bAutoWidth : false,
					});
	var header_inputs = $("#datatableEmployee thead input");
	header_inputs.on('keyup', function() {
		/* Filter on the column (the index) of this element */
		oTable.fnFilter(this.value, header_inputs.index(this));
	}).on('focus', function() {
		if (this.className == "search_init") {
			this.className = "";
			this.value = "";
		}
	}).on('blur', function(i) {
		if (this.value == "") {
			this.className = "search_init";
			this.value = asInitVals[header_inputs.index(this)];
		}
	});
	header_inputs.each(function(i) {
		asInitVals[i] = this.value;
	});
}

function projEmpTableSettings() {
	var asInitVals = [];
	var oTable = $('#datatableProjEmp')
			.dataTable(
					{
						"aaSorting" : [ [ 0, "asc" ] ],
						"sDom" : "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
						"sPaginationType" : "bootstrap",
						"oLanguage" : {
							"sSearch" : "",
							"sLengthMenu" : '_MENU_'
						},
						"aoColumnDefs" : [ {
							'sWidth' : '20%',
							'aTargets' : [ 0 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 1 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 2 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 3 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 4 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 4 ]
						} ],
						bAutoWidth : false,
					});
	var header_inputs = $("#datatableProjEmp thead input");
	header_inputs.on('keyup', function() {
		/* Filter on the column (the index) of this element */
		oTable.fnFilter(this.value, header_inputs.index(this));
	}).on('focus', function() {
		if (this.className == "search_init") {
			this.className = "";
			this.value = "";
		}
	}).on('blur', function(i) {
		if (this.value == "") {
			this.className = "search_init";
			this.value = asInitVals[header_inputs.index(this)];
		}
	});
	header_inputs.each(function(i) {
		asInitVals[i] = this.value;
	});
}

function loadTableData() {
	$
			.ajax({
				type : 'GET',
				url : $urlPrefix + 'many-to-many/project-list',
				dataType : 'json',
				async : true,
				success : function(result) {
					$
							.each(
									result,
									function(index, element) {
										$('#projectTableData')
												.append(
														'<tr>' + '<td>'
																+ element.projCode
																+ '</td>'
																+ '<td>'
																+ element.projCtgy
																+ '</td>'
																+ '<td>'
																+ element.projName
																+ '</td>'
																+ '<td>'
																+ formatDate(
																		element.dateStart,
																		'm/d/Y')
																+ '</td>'
																+ '<td>'
																+ formatDate(
																		element.dateAccmphld,
																		'm/d/Y')
																+ '</td>'
																+ '<td>'
																+ '<button id="upprj'
																+ element.projID
																+ '" type="button" class="btn btn-primary btn-lg" style="width: 60px;margin-right:5px" onclick="editSelectedProjectDetails('
																+ element.projID
																+ ')">Update</button>'
																+ '<button id="delprj'
																+ element.projID
																+ '" type="button" class="btn btn-danger btn-lg" style="width: 60px" onclick="deleteSelectedProjectData('
																+ element.projID
																+ ')">Delete</button>'
																+ '</td></tr>');

										$('#projectList').append(
												'<option value="'
														+ element.projID + '">'
														+ element.projName
														+ '</option>');

									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + " " + jqXHR.responseText);
				}
			});

	$
			.ajax({
				type : 'GET',
				url : $urlPrefix + 'many-to-many/employee-list',
				dataType : 'json',
				async : true,
				success : function(result) {
					$
							.each(
									result,
									function(index, element) {
										$('#employeeTableData')
												.append(
														'<tr>' + '<td>'
																+ element.empNo
																+ '</td>'
																+ '<td>'
																+ element.firstName
																+ ' '
																+ element.middleName
																+ ' '
																+ element.lastName
																+ '</td>'
																+ '<td>'
																+ element.jobPosition
																+ '</td>'
																+ '<td>'
																+ formatDate(
																		element.dateHired,
																		'm/d/Y')
																+ '</td>'
																+ '<td>'
																+ '<button id="upe'
																+ element.empID
																+ '" type="button" class="btn btn-primary btn-lg" style="width: 60px;margin-right:5px" onclick="editSelectedEmployeeDetails('
																+ element.empID
																+ ')">Update</button>'
																+ '<button id="dele'
																+ element.empID
																+ '" type="button" class="btn btn-danger btn-lg"  style="width: 60px"  onclick="deleteSelectedEmployeeData('
																+ element.empID
																+ ')">Delete</button>'
																+ '</td></tr>');

										$('#employeeList').append(
												'<option value="'
														+ element.empID + '">'
														+ element.firstName
														+ ' '
														+ element.lastName
														+ '</option>');

									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + " " + jqXHR.responseText);
				}
			});

	$
			.ajax({
				type : 'GET',
				url : $urlPrefix + 'many-to-many/project-employee-list',
				dataType : 'json',
				async : true,
				success : function(result) {
					$
							.each(
									result,
									function(index, element) {
										$('#projEmpTableData')
												.append(
														'<tr>' + '<td>'
																+ element.projCode
																+ '</td>'
																+ '<td>'
																+ element.projName
																+ '</td>'
																+ '<td>'
																+ element.empNo
																+ '</td>'
																+ '<td>'
																+ element.firstName
																+ ' '
																+ element.middleName
																+ ' '
																+ element.lastName
																+ '</td>'
																+ '<td>'
																+ '<button id="upprje'
																+ element.projID
																+ '" type="button" class="btn btn-primary btn-lg" style="width: 60px;margin-right:5px" onclick="editSelectedProjectDetails('
																+ element.projID
																+ ')">Update</button>'
																+ '<button id="delprje'
																+ element.projID
																+ '" type="button" class="btn btn-danger btn-lg" style="width: 60px" onclick="deleteSelectedProjectData('
																+ element.projID
																+ ')">Delete</button>'
																+ '</td></tr>');

									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + " " + jqXHR.responseText);
				}
			});

}

function formValidationsProject() {
	$('#projectForm')
			.bootstrapValidator(
					{
						fields : {
							projCtgy : {
								validators : {
									notEmpty : {
										message : 'The project category is required and can\'t be empty'
									}
								}
							},
							projName : {
								validators : {
									notEmpty : {
										message : 'The project name is required and can\'t be empty'
									}
								}
							},
							dateStart : {
								validators : {
									notEmpty : {
										message : 'The date start is required and can\'t be empty'
									}
								}
							},
							dateAccmphld : {
								validators : {
									notEmpty : {
										message : 'The date accomplished is required and can\'t be empty'
									}
								}
							}
						}
					}).on(
					'submit',
					function(event) {
						event.preventDefault();
						if ($('#projCtgy').val().length === 0
								|| $('#projName').val().length === 0
								|| $('#dateStart').val().length === 0
								|| $('#dateAccmphld').val().length === 0) {
							return;
						}
						saveProject();
					});
}

function formValidationsEmployee() {
	$('#employeeForm')
			.bootstrapValidator(
					{
						fields : {
							firstName : {
								validators : {
									notEmpty : {
										message : 'The first name is required and can\'t be empty'
									}
								}
							},
							lastName : {
								validators : {
									notEmpty : {
										message : 'The last name is required and can\'t be empty'
									}
								}
							},
							jobPosition : {
								validators : {
									notEmpty : {
										message : 'The job position is required and can\'t be empty'
									}
								}
							},
							dateHired : {
								validators : {
									notEmpty : {
										message : 'The date hired is required and can\'t be empty'
									}
								}
							}
						}
					}).on(
					'submit',
					function(event) {
						event.preventDefault();
						if ($('#firstName').val().length === 0
								|| $('#lastName').val().length === 0
								|| $('#jobPosition').val().length === 0
								|| $('#dateHired').val().length === 0) {
							return;
						}
						saveEmployee();
					});
}

function formValidationsProjEmp() {
	$('#projEmpForm')
			.bootstrapValidator(
					{
						fields : {
							projectList : {
								validators : {
									notEmpty : {
										message : 'The project is required and can\'t be empty'
									}
								}
							},
							employeeList : {
								validators : {
									notEmpty : {
										message : 'The employee is required and can\'t be empty'
									}
								}
							}
						}
					}).on(
					'submit',
					function(event) {
						event.preventDefault();
						if ($('#projectList').val().length === 0
								|| $('#employeeList').val().length === 0) {
							return;
						}
						alert("called");
						linkProjEmp();
					});
}

function saveProject() {
	$jsonArray = {
		"projID" : $('#projID').val(),
		"projCode" : $('#projCode').val(),
		"projCtgy" : $('#projCtgy').val(),
		"projName" : $('#projName').val(),
		"dateStart" : new Date($('#dateStart').val()),
		"dateAccmphld" : new Date($('#dateAccmphld').val())
	};

	$.ajax({
		type : 'POST',
		url : $urlPrefix + 'many-to-many/save-project',
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify($jsonArray),
		async : true,
		success : function(result) {
			if (result === "success") {
				alert("Data has been successfully saved!");
				location.reload();
			} else {
				alert("Unable to save. Project already exist!");
				$("#projCtgy").focus();
				$("html, body").animate({
					scrollTop : 0
				}, "slow");
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
		}
	});
}

function saveEmployee() {
	$jsonArray = {
		"empID" : $('#empID').val(),
		"empNo" : $('#empNo').val(),
		"firstName" : $('#firstName').val(),
		"middleName" : $('#middleName').val(),
		"lastName" : $('#lastName').val(),
		"jobPosition" : $('#jobPosition').val(),
		"dateHired" : new Date($('#dateHired').val())
	};

	$.ajax({
		type : 'POST',
		url : $urlPrefix + 'many-to-many/save-employee',
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify($jsonArray),
		async : true,
		success : function(result) {
			if (result === "success") {
				alert("Data has been successfully saved!");
				location.reload();
			} else {
				alert("Unable to save. Employee already exist!");
				$("#firstName").focus();
				$("#employeerow").animate({
					scrollTop : 0
				}, "slow");
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
		}
	});
}

function linkProjEmp() {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'many-to-many/link-project-employee/' + $('#projectList').val()+'/'+$('#employeeList').val(),
		async : true,
		dataType : 'json',
		success : function(result) {
			if (result === "success") {
				alert("Data has been successfully saved!");
				location.reload();
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
		}
	});
}


function editSelectedProjectDetails($id) {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'many-to-many/get-project-by-id/' + $id,
		async : true,
		dataType : 'json',
		success : function(result) {
			$.each(result, function(index, element) {
				$('#projID').val(element.projID);
				$('#projCode').val(element.projCode);
				$('#projCtgy').val(element.projCtgy);
				$('#projName').val(element.projName);
				$('#dateStart').val(formatDate(element.dateStart, 'm/d/Y'));
				$('#dateAccmphld').val(
						formatDate(element.dateAccmphld, 'm/d/Y'));
				$("html, body").animate({
					scrollTop : 0
				}, "slow");
				$('#projCtgy').focus();
			});
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});

}

function editSelectedEmployeeDetails($id) {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'many-to-many/get-employee-by-id/' + $id,
		async : true,
		dataType : 'json',
		success : function(result) {
			$.each(result, function(index, element) {
				$('#empID').val(element.empID);
				$('#empNo').val(element.empNo);
				$('#firstName').val(element.firstName);
				$('#middleName').val(element.middleName);
				$('#lastName').val(element.lastName);
				$('#jobPosition').val(element.jobPosition);
				$('#dateHired').val(formatDate(element.dateHired, 'm/d/Y'));
				$("#employeerow").animate({
					scrollTop : 0
				}, "slow");
				$('#firstName').focus();
			});
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});

}

function deleteSelectedProjectData($id) {
	if (confirm("delete selected Data?")) {
		$
				.ajax({
					type : 'GET',
					url : $urlPrefix + 'many-to-many/delete-project-by-id/'
							+ $id,
					async : true,
					success : function(result) {
						if (result == "success") {
							alert("Selected data has been deleted!");
							location.reload();
						} else {
							alert("Unable to delete! Data has been used in a transaction.");
						}
					},
					error : function(jqXHR, textStatus, errorThrown) {
						alert(jqXHR.status + " " + jqXHR.responseText);
					}
				});
	}

}

function generateProjectNo() {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'many-to-many/latest-projid',
		async : true,
		success : function(result) {
			$('#projCode').val(generateCode(result + 1, 9));
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});
}

function generateEmployeeNo() {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'many-to-many/latest-empid',
		async : true,
		success : function(result) {
			$('#empNo').val(generateCode(result + 1, 9));
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});
}

function deleteSelectedEmployeeData($id) {
	if (confirm("delete selected Data?")) {
		$
				.ajax({
					type : 'GET',
					url : $urlPrefix + 'many-to-many/delete-employee-by-id/'
							+ $id,
					async : true,
					success : function(result) {
						if (result == "success") {
							alert("Selected data has been deleted!");
							location.reload();
						} else {
							alert("Unable to delete! Data has been used in a transaction.");
						}
					},
					error : function(jqXHR, textStatus, errorThrown) {
						alert(jqXHR.status + " " + jqXHR.responseText);
					}
				});
	}

}
