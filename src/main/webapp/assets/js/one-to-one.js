function TimePicker() {
	$('#dob').datepicker({});
	$('#issuedDate').datepicker({});
	$('#validTil').datepicker({});
}

function MakeSelect2() {
	$('select').select2();
	$('.dataTables_filter').each(function() {
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}

function loadTableSettings() {
	tableSettings();
	LoadSelect2Script(MakeSelect2);
}

function tableSettings() {
	var asInitVals = [];
	var oTable = $('#datatable')
			.dataTable(
					{
						"aaSorting" : [ [ 0, "asc" ] ],
						"sDom" : "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
						"sPaginationType" : "bootstrap",
						"oLanguage" : {
							"sSearch" : "",
							"sLengthMenu" : '_MENU_'
						},
						"aoColumnDefs" : [ {
							'sWidth' : '20%',
							'aTargets' : [ 0 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 1 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 2 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 3 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 4 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 5 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 2 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 3 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 4 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 5 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 6 ]
						} ],
						bAutoWidth : false,
					});
	var header_inputs = $("#datatable thead input");
	header_inputs.on('keyup', function() {
		/* Filter on the column (the index) of this element */
		oTable.fnFilter(this.value, header_inputs.index(this));
	}).on('focus', function() {
		if (this.className == "search_init") {
			this.className = "";
			this.value = "";
		}
	}).on('blur', function(i) {
		if (this.value == "") {
			this.className = "search_init";
			this.value = asInitVals[header_inputs.index(this)];
		}
	});
	header_inputs.each(function(i) {
		asInitVals[i] = this.value;
	});
}

function loadTableData() {
	$
			.ajax({
				type : 'GET',
				url : $urlPrefix + 'one-to-one/person-list',
				dataType : 'json',
				async : true,
				success : function(result) {
					$
							.each(
									result,
									function(index, element) {

										$('#table_data')
												.append(
														'<tr><td>'
																+ element.firstName
																+ ' '
																+ element.middleName
																+ ' '
																+ element.lastName
																+ '</td><td>'
																+ result[index].passport.passportNo
																+ '</td><td>'
																+ element.dob
																+ '</td><td>'
																+ element.pob
																+ '</td><td>'
																+ result[index].passport.issuedDate
																+ '</td><td>'
																+ result[index].passport.validTil
																+ '</td>'
																+ '<td>'
																+ '<button id="up'
																+ element.personID
																+ '" type="button" class="btn btn-primary btn-lg" style="width: 60px" onclick="editSelectedDetails('
																+ element.personID
																+ ')">Update</button>'
																+ '<button id="del'
																+ element.personID
																+ '" type="button" class="btn btn-danger btn-lg" style="width: 60px" onclick="deleteSelectedData('
																+ element.personID
																+ ')">Delete</button>'
																+ '</td></tr>');
									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + " " + jqXHR.responseText);
				}
			});
}

function formValidations() {
	$('#defaultForm')
			.bootstrapValidator(
					{
						fields : {
							firstName : {
								validators : {
									notEmpty : {
										message : 'The first name is required and can\'t be empty'
									}
								}
							},
							lastName : {
								validators : {
									notEmpty : {
										message : 'The last name is required and can\'t be empty'
									}
								}
							},
							dob : {
								validators : {
									notEmpty : {
										message : 'The date of birth is required and can\'t be empty'
									}
								}
							},
							pob : {
								validators : {
									notEmpty : {
										message : 'The place of birth is required and can\'t be empty'
									}
								}
							},
							issuedDate : {
								validators : {
									notEmpty : {
										message : 'The date is required and can\'t be empty'
									}
								}
							},
							validTil : {
								validators : {
									notEmpty : {
										message : 'The date is required and can\'t be empty'
									}
								}
							}
						}
					}).on(
					'submit',
					function(event) {
						event.preventDefault();
						if ($('#firstName').val().length === 0
								|| $('#lastName').val().length === 0
								|| $('#dob').val().length === 0
								|| $('#pob').val().length === 0
								|| $('#issuedDate').val().length === 0
								|| $('#validTil').val().length === 0) {
							return;
						}
						savePersonPassport();
					});
}

function generatePassportNo() {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'one-to-one/latest-passid',
		async : true,
		success : function(result) {
			$('#passportNo').val(generateCode(result + 1, 9));
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});
}

function savePersonPassport() {
	$jsonArray = {
		"personID" : $('#personID').val(),
		"firstName" : $('#firstName').val(),
		"middleName" : $('#middleName').val(),
		"lastName" : $('#lastName').val(),
		"dob" : new Date($('#dob').val()),
		"pob" : $('#pob').val(),
		"passport" : {
			"passportID" : $('#passportID').val(),
			"passportNo" : $('#passportNo').val(),
			"issuedDate" : new Date($('#issuedDate').val()),
			"validTil" : new Date($('#validTil').val())
		}
	};

	$.ajax({
		type : 'POST',
		url : $urlPrefix + 'one-to-one/save-personpassport',
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify($jsonArray),
		async : true,
		success : function(result) {
			if (result === "success") {
				alert("Data has been successfully saved!");
				location.reload();
			} else {
				alert("Unable to save. Person name already exist!");
				$("#firstName").focus();
				$("html, body").animate({
					scrollTop : 0
				}, "slow");
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
		}
	});
}

function editSelectedDetails($id) {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'one-to-one/get-person-by-id/' + $id,
		async : true,
		dataType : 'json',
		success : function(result) {
			$.each(result, function(index, element) {
				$('#personID').val(element.personID);
				$('#passportID').val(result[index].passport.passportID);
				$('#firstName').val(element.firstName);
				$('#middleName').val(element.middleName);
				$('#lastName').val(element.lastName);
				$('#dob').val(element.dob);
				$('#pob').val(element.pob);

				$('#passportNo').val(result[index].passport.passportNo);
				$('#issuedDate').val(result[index].passport.issuedDate);
				$('#validTil').val(result[index].passport.validTil);

				$("html, body").animate({
					scrollTop : 0
				}, "slow");

				$('#firstName').focus();
			});
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});

}

function deleteSelectedData($id) {
	if (confirm("delete selected Data?")) {
		$.ajax({
			type : 'GET',
			url : $urlPrefix + 'one-to-one/delete-person-by-id/' + $id,
			async : true,
			success : function(result) {
				if (result == "success") {
					alert("Selected data has been deleted!");
					location.reload();
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.status + " " + jqXHR.responseText);
			}
		});
	}

}