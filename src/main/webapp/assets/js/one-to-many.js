function cancelTrans($buttonId) {
	$("#" + $buttonId).click(function() {
		if (confirm("Cancel Transaction?")) {
			location.reload();
		}
	});
}
function selectSettings() {
	$('#category').select2();
}

function MakeSelect2() {
	$('select').select2();
	$('.dataTables_filter').each(function() {
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}

function loadTableSettings() {
	categoryTableSettings();
	productTableSettings();
	LoadSelect2Script(MakeSelect2);
}

function categoryTableSettings() {
	var asInitVals = [];
	var oTable = $('#datatableCategory')
			.dataTable(
					{
						"aaSorting" : [ [ 0, "asc" ] ],
						"sDom" : "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
						"sPaginationType" : "bootstrap",
						"oLanguage" : {
							"sSearch" : "",
							"sLengthMenu" : '_MENU_'
						},
						"aoColumnDefs" : [ {
							'sWidth' : '70%',
							'aTargets' : [ 0 ]
						}, {
							'sWidth' : '30%',
							'aTargets' : [ 1 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 1 ]
						} ],
						bAutoWidth : false,
					});
	var header_inputs = $("#datatableCategory thead input");
	header_inputs.on('keyup', function() {
		/* Filter on the column (the index) of this element */
		oTable.fnFilter(this.value, header_inputs.index(this));
	}).on('focus', function() {
		if (this.className == "search_init") {
			this.className = "";
			this.value = "";
		}
	}).on('blur', function(i) {
		if (this.value == "") {
			this.className = "search_init";
			this.value = asInitVals[header_inputs.index(this)];
		}
	});
	header_inputs.each(function(i) {
		asInitVals[i] = this.value;
	});
}

function productTableSettings() {
	var asInitVals = [];
	var oTable = $('#datatableProduct')
			.dataTable(
					{
						"aaSorting" : [ [ 0, "asc" ] ],
						"sDom" : "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
						"sPaginationType" : "bootstrap",
						"oLanguage" : {
							"sSearch" : "",
							"sLengthMenu" : '_MENU_'
						},
						"aoColumnDefs" : [ {
							'sWidth' : '20%',
							'aTargets' : [ 0 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 1 ]
						}, {
							'sWidth' : '20%',
							'aTargets' : [ 2 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 3 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 4 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 5 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 6 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 3 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 4 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 5 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 6 ]
						} ],

						bAutoWidth : false,
					});
	var header_inputs = $("#datatableProduct thead input");
	header_inputs.on('keyup', function() {
		/* Filter on the column (the index) of this element */
		oTable.fnFilter(this.value, header_inputs.index(this));
	}).on('focus', function() {
		if (this.className == "search_init") {
			this.className = "";
			this.value = "";
		}
	}).on('blur', function(i) {
		if (this.value == "") {
			this.className = "search_init";
			this.value = asInitVals[header_inputs.index(this)];
		}
	});
	header_inputs.each(function(i) {
		asInitVals[i] = this.value;
	});
}

function loadTableData() {
	$
			.ajax({
				type : 'GET',
				url : $urlPrefix + 'one-to-many/category-list',
				dataType : 'json',
				async : true,
				success : function(result) {
					$
							.each(
									result,
									function(index, element) {
										$('#categoryTableData')
												.append(
														'<tr><td>'
																+ element.ctgyName
																+ '</td>'
																+ '<td>'
																+ '<button id="up'
																+ element.ctgyID
																+ '" type="button" class="btn btn-primary btn-lg" style="width: 60px;margin-right:5px" onclick="editSelectedCategoryDetails('
																+ element.ctgyID
																+ ')">Update</button>'
																+ '<button id="del'
																+ element.ctgyID
																+ '" type="button" class="btn btn-danger btn-lg" style="width: 60px" onclick="deleteSelectedCategoryData('
																+ element.ctgyID
																+ ')">Delete</button>'
																+ '</td></tr>');

										$('#category').append(
												'<option value="'
														+ element.ctgyID + '">'
														+ element.ctgyName
														+ '</option>');

									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + " " + jqXHR.responseText);
				}
			});

	$
			.ajax({
				type : 'GET',
				url : $urlPrefix + 'one-to-many/product-list',
				dataType : 'json',
				async : true,
				success : function(result) {
					$
							.each(
									result,
									function(index, element) {
										$('#productTableData')
												.append(
														'<tr>' + '<td>'
																+ element.ctgyName
																+ '</td>'
																+ '<td>'
																+ element.prodCode
																+ '</td>'
																+ '<td>'
																+ element.prodName
																+ '</td>'
																+ '<td align="right">'
																+ formatDecimalValue(element.prodCost)
																+ '</td>'
																+ '<td align="right">'
																+ formatDecimalValue(element.sellingPrice)
																+ '</td>'
																+ '<td align="right">'
																+ formatDecimalValue(element.orderPoint)
																+ '</td>'
																+ '<td>'
																+ '<button id="upp'
																+ element.prodID
																+ '" type="button" class="btn btn-primary btn-lg" style="width: 60px;margin-right:5px" onclick="editSelectedProductDetails('
																+ element.prodID
																+ ')">Update</button>'
																+ '<button id="delp'
																+ element.prodID
																+ '" type="button" class="btn btn-danger btn-lg"  style="width: 60px"  onclick="deleteSelectedProductData('
																+ element.prodID
																+ ')">Delete</button>'
																+ '</td></tr>');

									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + " " + jqXHR.responseText);
				}
			});

}

function formValidationsCategory() {
	$('#categoryForm')
			.bootstrapValidator(
					{
						fields : {
							ctgyName : {
								validators : {
									notEmpty : {
										message : 'The category is required and can\'t be empty'
									}
								}
							}
						}
					}).on('submit', function(event) {
				event.preventDefault();
				if ($('#ctgyName').val().length === 0) {
					return;
				}
				saveCategory();
			});
}

function formValidationsProduct() {
	$('#productForm')
			.bootstrapValidator(
					{
						fields : {
							category : {
								validators : {
									notEmpty : {
										message : 'The category is required and can\'t be empty'
									}
								}
							},
							prodName : {
								validators : {
									notEmpty : {
										message : 'The product name is required and can\'t be empty'
									}
								}
							},
							prodCost : {
								validators : {
									notEmpty : {
										message : 'The product cost is required and can\'t be empty'
									}
								}
							},
							sellingPrice : {
								validators : {
									notEmpty : {
										message : 'The selling price is required and can\'t be empty'
									}
								}
							},
							orderPoint : {
								validators : {
									notEmpty : {
										message : 'The order point is required and can\'t be empty'
									}
								}
							}
						}
					}).on(
					'submit',
					function(event) {
						event.preventDefault();
						if ($('#category').val().length === 0
								|| $('#prodName').val().length === 0
								|| $('#prodCost').val().length === 0
								|| $('#sellingPrice').val().length === 0
								|| $('#orderPoint').val().length === 0) {
							return;
						}
						saveProduct();
					});
}

function saveCategory() {
	$jsonArray = {
		"ctgyID" : $('#ctgyID').val(),
		"ctgyName" : $('#ctgyName').val()
	};

	$.ajax({
		type : 'POST',
		url : $urlPrefix + 'one-to-many/save-category',
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify($jsonArray),
		async : true,
		success : function(result) {
			if (result === "success") {
				alert("Data has been successfully saved!");
				location.reload();
			} else {
				alert("Unable to save. Category already exist!");
				$("#ctgyName").focus();
				$("html, body").animate({
					scrollTop : 0
				}, "slow");
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
		}
	});
}

function saveProduct() {
	$jsonArray = {
		"prodID" : $('#prodID').val(),
		"prodCode" : $('#prodCode').val(),
		"prodName" : $('#prodName').val(),
		"prodCost" : $('#prodCost').val(),
		"sellingPrice" : $('#sellingPrice').val(),
		"orderPoint" : $('#orderPoint').val(),
		"category" : {
			"ctgyID" : $('#category').val()
		}
	};

	$.ajax({
		type : 'POST',
		url : $urlPrefix + 'one-to-many/save-product',
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify($jsonArray),
		async : true,
		success : function(result) {
			if (result === "success") {
				alert("Data has been successfully saved!");
				location.reload();
			} else {
				alert("Unable to save. Product already exist!");
				$("#prodCode").focus();
				$("#productrow").animate({
					scrollTop : 0
				}, "slow");
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
		}
	});
}
function editSelectedCategoryDetails($id) {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'one-to-many/get-category-by-id/' + $id,
		async : true,
		dataType : 'json',
		success : function(result) {
			$.each(result, function(index, element) {
				$('#ctgyID').val(element.ctgyID);
				$('#ctgyName').val(element.ctgyName);
				$("html, body").animate({
					scrollTop : 0
				}, "slow");
				$('#ctgyName').focus();
			});
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});

}

function editSelectedProductDetails($id) {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'one-to-many/get-product-by-id/' + $id,
		async : true,
		dataType : 'json',
		success : function(result) {
			$.each(result, function(index, element) {
				$('#category').val(element.ctgyID);
				LoadSelect2Script(selectSettings);
				$('#prodID').val(element.prodID);
				$('#prodCode').val(element.prodCode);
				$('#prodName').val(element.prodName);
				$('#prodCost').val(element.prodCost);
				$('#sellingPrice').val(element.sellingPrice);
				$('#orderPoint').val(element.orderPoint);
				$("#productrow").animate({
					scrollTop : 0
				}, "slow");
				$('#category').focus();

			});
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});

}

function deleteSelectedCategoryData($id) {
	if (confirm("delete selected Data?")) {
		$
				.ajax({
					type : 'GET',
					url : $urlPrefix + 'one-to-many/delete-category-by-id/'
							+ $id,
					async : true,
					success : function(result) {
						if (result == "success") {
							alert("Selected data has been deleted!");
							location.reload();
						} else {
							alert("Unable to delete! Data has been used in a transaction.");
						}
					},
					error : function(jqXHR, textStatus, errorThrown) {
						alert(jqXHR.status + " " + jqXHR.responseText);
					}
				});
	}

}

function deleteSelectedProductData($id) {
	if (confirm("delete selected Data?")) {
		$.ajax({
			type : 'GET',
			url : $urlPrefix + 'one-to-many/delete-product-by-id/' + $id,
			async : true,
			success : function(result) {
				if (result == "success") {
					alert("Selected data has been deleted!");
					location.reload();
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.status + " " + jqXHR.responseText);
			}
		});
	}
}

function generateProductNo() {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'one-to-many/latest-prodid',
		async : true,
		success : function(result) {
			$('#prodCode').val(generateCode(result + 1, 9));
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText);
		}
	});
}
