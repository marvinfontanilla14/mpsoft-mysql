function validateUser() {
    $userCredentials = {
        "userName": $('#username').val(),
        "userPassword": $('#user_password').val()
    };

    $.ajax({
        type: 'POST',
        url: $urlPrefix + 'user-authentication/validate-user/',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify($userCredentials),
        async: true,
        success: function(result) {
            if (result === 'success') {
                $("#username").prop('readonly', true);
                $("#user_password").prop('readonly', true);
                $("#btn_save").prop('disabled', true);
                setUserSession($userCredentials);
            } else {
                $('#error_message').html('');
                $('#error_message').show();
                $('#error_message').append('Unable to login. Invalid Credentials or User has been deactivated!');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
        }
    });
}

function setUserSession($userCredentials) {
    $.ajax({
        type: 'POST',
        url: $urlPrefix + 'user-authentication/set-session/',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify($userCredentials),
        async: true,
        success: function(result) {
            window.location.href = 'main';
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
        }
    });
}