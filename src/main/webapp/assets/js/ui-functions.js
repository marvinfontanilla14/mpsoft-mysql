$(document).ready(function() {
	input_validation();
});

function generateCode($number, $lengthCount) {
	var $code = "";
	var $numberLength = $number.toString().length;
	if ($numberLength <= $lengthCount) {
		for (var i = 0; i < $lengthCount - $numberLength; i++) {
			$code += "0";
		}
		$code = $code + $number;
	} else {
		$code = $number;
	}
	return $code;
}

function input_validation() {
	// -- Email
	$('input.email').on('keypress', function(e) {
		return !(e.which != 8 // backspace
				&& e.which != 13 // enter
				&& e.which != 0 // null
				&& (e.which < 48 || e.which > 57) // 0-9
				&& e.which != 46 // .
				&& e.which != 64 // @
				&& e.which != 95 // _
				&& (e.which < 65 || e.which > 90) // A-Z
		&& (e.which < 97 || e.which > 122) // a-z
		);
	});

	// -- Account (Username/Password)
	$('input.account').on('keypress', function(e) {
		return !(e.which != 8 // backspace
				&& e.which != 13 // enter
				&& e.which != 0 // null
				&& (e.which < 48 || e.which > 57) // 0-9
				&& e.which != 95 // _
				&& (e.which < 65 || e.which > 90) // A-Z
		&& (e.which < 97 || e.which > 122) // a-z
		);
	});

	// -- numeric
	$('input.numeric').on('keypress', function(e) {
		return !(e.which != 8 // backspace
				&& e.which != 13 // enter
				&& e.which != 0 // null
		&& (e.which < 48 || e.which > 57) // 0-9
		);
	});

	// -- decimal
	$('input.decimal').on('keypress', function(e) {
		return !(e.which != 8 // backspace
				&& e.which != 13 // enter
				&& e.which != 0 // null
				&& e.which != 46 // .
		&& (e.which < 48 || e.which > 57) // 0-9
		);
	});
}

function formatDecimalValue(number) {
	number = Number(number).toFixed(2);
	var n = number.toString().split(".");
	n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return n.join(".");
}
function formatIntegerValue(number) {
	number = number.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return number;
}

String.prototype.replaceAll = function(target, replacement) {
	return this.split(target).join(replacement);
};

function removeComma(number) {
	return number.replaceAll(",", "");
}

function formatDate($strDate, $strFormat) {
	var $date = new Date($strDate);
	return $date.format($strFormat);
}
