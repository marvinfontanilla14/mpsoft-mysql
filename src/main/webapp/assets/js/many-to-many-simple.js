function MakeSelect2() {
	$('select').select2();
	$('.dataTables_filter').each(function() {
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}

function loadTableSettings() {
	tableSettings();
	LoadSelect2Script(MakeSelect2);
}

function tableSettings() {
	var asInitVals = [];
	var oTable = $('#datatable')
			.dataTable(
					{
						"aaSorting" : [ [ 0, "asc" ] ],
						"sDom" : "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
						"sPaginationType" : "bootstrap",
						"oLanguage" : {
							"sSearch" : "",
							"sLengthMenu" : '_MENU_'
						},
						"aoColumnDefs" : [ {
							'sWidth' : '40%',
							'aTargets' : [ 0 ]
						}, {
							'sWidth' : '40%',
							'aTargets' : [ 1 ]
						}, {
							'sWidth' : '10%',
							'aTargets' : [ 2 ]
						}, {
							'bSortable' : false,
							'aTargets' : [ 2 ]
						} ],
						bAutoWidth : false,
					});
	var header_inputs = $("#datatable thead input");
	header_inputs.on('keyup', function() {
		/* Filter on the column (the index) of this element */
		oTable.fnFilter(this.value, header_inputs.index(this));
	}).on('focus', function() {
		if (this.className == "search_init") {
			this.className = "";
			this.value = "";
		}
	}).on('blur', function(i) {
		if (this.value == "") {
			this.className = "search_init";
			this.value = asInitVals[header_inputs.index(this)];
		}
	});
	header_inputs.each(function(i) {
		asInitVals[i] = this.value;
	});
}

function loadTableData() {
	$
			.ajax({
				type : 'GET',
				url : $urlPrefix + 'many-to-many-simple/gamer-list',
				dataType : 'json',
				async : true,
				success : function(result) {
					$
							.each(
									result,
									function(index, element) {

										$('#table_data')
												.append(
														'<tr><td>'
																+ element.gamerName
																+ '</td><td>'
																+ element.gameName
																+ '</td>'
																+ '<td>'
																+ '<button id="del'
																+ element.gamerID
																+ '" type="button" class="btn btn-danger btn-lg" style="width: 60px" onclick="deleteSelectedData('
																+ element.gamerID
																+ ')">Delete</button>'
																+ '</td></tr>');
									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + " " + jqXHR.responseText);
				}
			});
}

function formValidations() {
	$('#defaultForm')
			.bootstrapValidator(
					{
						fields : {
							gamerName : {
								validators : {
									notEmpty : {
										message : 'The gamer name is required and can\'t be empty'
									}
								}
							},
							gameList : {
								validators : {
									notEmpty : {
										message : 'The game list is required and can\'t be empty'
									}
								}
							}
						}
					}).on(
					'submit',
					function(event) {
						event.preventDefault();
						if ($('#gamerName').val().length === 0
								|| $('#gameList').val().length === 0) {
							return;
						}
						saveGamer();
					});
}

function saveGamer() {
	$.ajax({
		type : 'GET',
		url : $urlPrefix + 'many-to-many-simple/save-gamer/'
				+ $('#gamerName').val() + '/' + $('#gameList').val(),
		async : true,
		success : function(result) {
			if (result === "success") {
				alert("Data has been successfully saved!");
				location.reload();
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + " " + jqXHR.responseText + errorThrown);
		}
	});
}

function deleteSelectedData($id) {
	if (confirm("delete selected Data?")) {
		$.ajax({
			type : 'GET',
			url : $urlPrefix + 'many-to-many-simple/delete-gamer-by-id/' + $id,
			async : true,
			success : function(result) {
				if (result == "success") {
					alert("Selected data has been deleted!");
					location.reload();
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.status + " " + jqXHR.responseText);
			}
		});
	}

}