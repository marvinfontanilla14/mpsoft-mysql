<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:bundle basename="application">
	<fmt:message key="url.prefix" var="urlPrefix" />
</fmt:bundle>
<html>

<head>
<%@ page isELIgnored="false"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MPSoft</title>
<script>
	$urlPrefix = "${urlPrefix}";
</script>
</head>


<body>
	<h1>MAIN PAGE</h1>
	<h1>Hello! ${SYSTEMUSER}</h1>
	<a href="<c:url value='/user-logout' />">Logout</a>
</body>

</html>