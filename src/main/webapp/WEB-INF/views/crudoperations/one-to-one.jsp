<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmt:bundle basename="application">
	<fmt:message key="url.prefix" var="urlPrefix" />
</fmt:bundle>

<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="main">Dashboard</a></li>
			<li><a href="#">CRUD Operations</a></li>
			<li><a href="#">One to One Relationship</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span>One to One Relationship</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
					</a> <a class="expand-link"> <i class="fa fa-expand"></i>
					</a> <a class="close-link"> <i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" class="form-horizontal">
					<fieldset>
						<legend>Person Details</legend>
						<div class="form-group">
							<input type="hidden" name="personID" id="personID" /> <input
								type="hidden" name="passportID" id="passportID" /> <label
								class="col-sm-3 control-label">First Name</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="firstName"
									id="firstName" maxlength="25" placeholder="Enter First Name here" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Middle Name</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="middleName"
									id="middleName" maxlength="25" placeholder="Enter Middle Name here"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Last Name</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="lastName"
									id="lastName" maxlength="25" placeholder="Enter Last Name here"/>
							</div>
						</div>
						<div class="form-group">
							<label for="date_example" class="col-sm-3 control-label">Date
								of Birth</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" name="dob" id="dob"
									placeholder="Enter Date of Birth" maxlength="10">
							</div>
						</div>
						<div class="form-group">
							<label for="date_example" class="col-sm-3 control-label">Place
								of Birth</label>
							<div class="col-sm-4">
								<textarea id="pob" name="pob" cols="30" rows="10"
									class="form-control" placeholder="Enter Place of Birth here"></textarea>
							</div>
						</div>

					</fieldset>
					<fieldset>
						<legend>Passport Details</legend>
						<div class="form-group">
							<label class="col-sm-3 control-label">Passport No</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" name="passportNo"
									id="passportNo" />
							</div>
						</div>
						<div class="form-group">
							<label for="date_example" class="col-sm-3 control-label">Date</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" name="issuedDate"
									id="issuedDate" placeholder="Date Issued" maxlength="10">
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control" name="validTil"
									id="validTil" placeholder="Valid Till" maxlength="10">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button id="btnsave" type="submit" class="btn btn-primary">Save</button>
								<button id="btncancel" type="button" class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</fieldset>

					<fieldset>
						<legend>List</legend>
						<div class="box-content no-padding table-responsive">
							<table
								class="table table-bordered table-striped table-hover table-heading table-datatable"
								id="datatable">
								<thead>
									<tr>
										<th><label><input type="text" name="search_name"
												value="Search Name" class="search_init" /></label></th>
										<th><label><input type="text"
												name="search_passno" value="Search Passport No"
												class="search_init" /></label></th>
										<th><label>Date of Birth</label></th>
										<th><label>Place of Birth</label></th>
										<th><label>Date Issued</label></th>
										<th><label>Valid Till</label></th>
										<th></th>
									</tr>
								</thead>

								<tbody id="table_data">

								</tbody>

								<tfoot>
									<tr>
										<th>Name</th>
										<th>Passport No</th>
										<th>Date of Birth</th>
										<th>Place of Birth</th>
										<th>Date Issued</th>
										<th>Valid Till</th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="assets/js/one-to-one.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#passportNo").prop('readonly', true);
		$urlPrefix = "${urlPrefix}";
		$("#btncancel").click(function() {
			if (confirm("Cancel Transaction?")) {
				location.reload();
			}
		});
		// Load Timepicker plugin
		LoadTimePickerScript(TimePicker);
		// Add tooltip to form-controls
		$('.form-control').tooltip();
		LoadBootstrapValidatorScript(formValidations);
		// Add drag-n-drop feature to boxes
		WinMove();
		loadTableData();
		LoadDataTablesScripts(loadTableSettings);
		generatePassportNo();
	});
</script>

