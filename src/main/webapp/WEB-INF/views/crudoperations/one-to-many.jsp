<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmt:bundle basename="application">
	<fmt:message key="url.prefix" var="urlPrefix" />
</fmt:bundle>

<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="main">Dashboard</a></li>
			<li><a href="#">CRUD Operations</a></li>
			<li><a href="#">One to Many Relationship</a></li>
		</ol>
	</div>
</div>
<!-- CATEGORY FORM -->
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span>Product Category Form</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
					</a> <a class="expand-link"> <i class="fa fa-expand"></i>
					</a> <a class="close-link"> <i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="categoryForm" method="post" class="form-horizontal">
					<fieldset>
						<legend>Product Category</legend>
						<div class="form-group">
							<input type="hidden" name="ctgyID" id="ctgyID" /> <label
								class="col-sm-3 control-label">Category</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="ctgyName"
									id="ctgyName" maxlength="25" placeholder="Enter Category here"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button id="btnsaveCategory" type="submit"
									class="btn btn-primary">Save</button>
								<button id="btncancelCategory" type="button"
									class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>List of Categories</legend>
						<div class="col-sm-8">
							<div class="box-content no-padding table-responsive">
								<table
									class="table table-bordered table-striped table-hover table-heading table-datatable"
									id="datatableCategory">
									<thead>
										<tr>
											<th><label><input type="text"
													name="search_category" value="Search Category"
													class="search_init" /></label></th>
											<th></th>
										</tr>
									</thead>

									<tbody id="categoryTableData">

									</tbody>

									<tfoot>
										<tr>
											<th>Category</th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- PRODUCT FORM -->
<div class="row" id ="productrow">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span>Product Form</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
					</a> <a class="expand-link"> <i class="fa fa-expand"></i>
					</a> <a class="close-link"> <i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="productForm" method="post" class="form-horizontal">
					<fieldset>
						<legend>Product</legend>
						<input type="hidden" name="prodID" id="prodID" />
						<div class="form-group">
							<label class="col-sm-3 control-label">Category</label>
							<div class="col-sm-4">
								<select class="populate placeholder" name="category"
									id="category">
									<option value="">-- Select a category --</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Product Code</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" name="prodCode"
									id="prodCode" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Product Name</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="prodName"
									id="prodName" maxlength="30" placeholder="Enter Product Name here"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Product Cost</label>
							<div class="col-sm-2">
								<input type="text" class="form-control decimal" name="prodCost"
									id="prodCost" maxlength="12" placeholder="Product Cost here"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Selling Price</label>
							<div class="col-sm-2">
								<input type="text" class="form-control decimal" name="sellingPrice"
									id="sellingPrice" maxlength="12" placeholder="Selling Price here"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Order Point</label>
							<div class="col-sm-2">
								<input type="text" class="form-control decimal" name="orderPoint"
									id="orderPoint" maxlength="12" placeholder="Order Point here"/>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button id="btnsaveProduct" type="submit" class="btn btn-primary">Save</button>
								<button id="btncancelProduct" type="button" class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>List of Products</legend>
						<div class="box-content no-padding table-responsive">
							<table
								class="table table-bordered table-striped table-hover table-heading table-datatable"
								id="datatableProduct">
								<thead>
									<tr>
										<th><label><input type="text" name="search_category"
												value="Search Category" class="search_init" /></label></th>
										<th><label><input type="text"
												name="search_prodcode" value="Search Product Code"
												class="search_init" /></label></th>
												<th><label><input type="text"
												name="search_prod" value="Search Product Name"
												class="search_init" /></label></th>
										<th><label>Product Cost</label></th>
										<th><label>Selling Price</label></th>
										<th><label>Order Point</label></th>
										<th></th>
									</tr>
								</thead>

								<tbody id="productTableData">

								</tbody>

								<tfoot>
									<tr>
										<th>Category</th>
										<th>Product Code</th>
										<th>Product Name</th>
										<th>Product Cost</th>
										<th>Selling Price</th>
										<th>Order Point</th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="assets/js/one-to-many.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#prodCode").prop('readonly', true);
		$urlPrefix = "${urlPrefix}";
		cancelTrans("btncancelCategory");
		cancelTrans("btncancelProduct");
		// Load Timepicker plugin
		LoadSelect2Script(selectSettings);
		// Add tooltip to form-controls
		$('.form-control').tooltip();
		LoadBootstrapValidatorScript(formValidationsCategory);
		LoadBootstrapValidatorScript(formValidationsProduct);
		// Add drag-n-drop feature to boxes
		WinMove();
		loadTableData();
		LoadDataTablesScripts(loadTableSettings);
		generateProductNo();
	});
</script>

