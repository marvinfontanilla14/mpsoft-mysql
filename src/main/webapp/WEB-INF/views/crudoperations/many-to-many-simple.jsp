<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmt:bundle basename="application">
	<fmt:message key="url.prefix" var="urlPrefix" />
</fmt:bundle>

<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="main">Dashboard</a></li>
			<li><a href="#">CRUD Operations</a></li>
			<li><a href="#">Many to Many Relationship</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span>Many to Many Relationship</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
					</a> <a class="expand-link"> <i class="fa fa-expand"></i>
					</a> <a class="close-link"> <i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" class="form-horizontal">
					<fieldset>
						<legend>Gamers - Games</legend>
						<div class="form-group">
							<input type="hidden" name="gamerID" id="gamerID" /> <label
								class="col-sm-3 control-label">Gamer Name</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="gamerName"
									id="gamerName" maxlength="30" placeholder="Enter Gamer Name here"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Games(Comma
								Separated)</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="gameList"
									id="gameList" maxlength="100" placeholder="Enter Game List Here"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button id="btnsave" type="submit" class="btn btn-primary">Save</button>
								<button id="btncancel" type="button" class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</fieldset>

					<fieldset>
						<legend>Gamers - Games List</legend>
						<div class="col-sm-9">
							<div class="box-content no-padding table-responsive">
								<table
									class="table table-bordered table-striped table-hover table-heading table-datatable"
									id="datatable">
									<thead>
										<tr>
											<th><label><input type="text"
													name="search_gamer" value="Search Gamer"
													class="search_init" /></label></th>
											<th><label><input type="text" name="search_game"
													value="Search Game" class="search_init" /></label></th>
											<th></th>
										</tr>
									</thead>

									<tbody id="table_data">

									</tbody>

									<tfoot>
										<tr>
											<th>Gamer</th>
											<th>Games</th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="assets/js/many-to-many-simple.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$urlPrefix = "${urlPrefix}";
		$("#btncancel").click(function() {
			if (confirm("Cancel Transaction?")) {
				location.reload();
			}
		});
		// Add tooltip to form-controls
		$('.form-control').tooltip();
		LoadBootstrapValidatorScript(formValidations);
		// Add drag-n-drop feature to boxes
		WinMove();
		loadTableData();
		LoadDataTablesScripts(loadTableSettings);
	});
</script>

