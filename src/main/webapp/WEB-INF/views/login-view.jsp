<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmt:bundle basename="application">
    <fmt:message key="url.prefix" var="urlPrefix" />
    <fmt:message key="system.title" var="systemTitle" />
</fmt:bundle>

<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>${systemTitle}</title>
        <link rel="shortcut icon" type="image/x-icon" href="javacon.ico" />        
        <link href="assets/plugins/bootstrap/bootstrap.css" rel="stylesheet">
        <link href="assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
        <link href="assets/css/font-awesome.css" rel="stylesheet">
        <link href='assets/css/font-family-righteous.css' rel='stylesheet' type='text/css'>
        <link href="assets/css/style.css" rel="stylesheet">
    </head>


    <body>
        <div id="page-login" class="row" style="margin-top: 5%">
            <div
                class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div class="box">
                    <div class="box-header">
                        <div class="box-name">
                            <span>&nbsp; MPSoft&trade; | Login</span>
                        </div>
                    </div>

                    <div class="box-content">

                        <form id="loginForm" method="post" class="form-horizontal">
                            <fieldset>
                                <p class="bg-danger text-center" id="error_message"></p>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Username</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="username" id="username" maxlength="25" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Password</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="user_password" id="user_password" maxlength="25" />
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-group">
                                <div class="col-sm-3 col-sm-offset-8">
                                    <button type="submit" class="btn btn-primary btn-block" id="btn_save">
                                        <i class="fa fa-sign-in"></i> Submit
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!--End Container-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="assets/plugins/jquery/jquery-2.1.0.min.js"></script>
        <script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
        <!-- All functions for this theme + document.ready processing -->
        <script src="assets/js/devoops.js"></script>
        <script type="text/javascript" src="assets/js/js-loader.js"></script>


        <script>
            function userAccountFormValidations() {
                $('#loginForm')
                        .bootstrapValidator(
                                {
                                    fields: {
                                        username: {
                                            validators: {
                                                notEmpty: {
                                                    message: 'The username is required and can\'t be empty'
                                                },
                                                stringLength: {
                                                    min: 3,
                                                    max: 25,
                                                    message: 'The username must be more than 3 and less than 25 characters long'
                                                },
                                                regexp: {
                                                    regexp: /^[a-zA-Z0-9_]+$/,
                                                    message: 'The username can only consist of alphabetical, number and underscore'
                                                }
                                            }
                                        },
                                        user_password: {
                                            validators: {
                                                notEmpty: {
                                                    message: 'The password is required and can\'t be empty'
                                                },
                                                stringLength: {
                                                    min: 3,
                                                    max: 25,
                                                    message: 'The password must be more than 3 and less than 25 characters long'
                                                },
                                                regexp: {
                                                    regexp: /^[a-zA-Z0-9_]+$/,
                                                    message: 'The password can only consist of alphabetical, number and underscore'
                                                }
                                            }
                                        }
                                    }
                                })
                        .on(
                                'keyup',
                                function() {
                                    if ($('#username').val().length === 0
                                            || $('#user_password').val().length === 0) {
                                        $("#btn_save").prop('disabled', true);
                                    }
                                }).on('submit', function(event) {
                    event.preventDefault();
                    validateUser();
                });
            }

            $(function() {
                $('#error_message').hide();
                $('#btn_save').prop('disabled', true);
                $urlPrefix = "${urlPrefix}";
                loadLoginScript();
                LoadBootstrapValidatorScript(userAccountFormValidations);
                // Add drag-n-drop feature to boxes
                WinMove();
            });
        </script>
    </body>

</html>