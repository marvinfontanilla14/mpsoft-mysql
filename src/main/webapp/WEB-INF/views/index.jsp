<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<fmt:bundle basename="application">
	<fmt:message key="url.prefix" var="urlPrefix" />
	<fmt:message key="system.title" var="systemTitle" />
</fmt:bundle>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>${systemTitle}</title>
<link rel="shortcut icon" type="image/x-icon" href="javacon.ico" />
<link href="assets/plugins/bootstrap/bootstrap.css" rel="stylesheet">
<link href="assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="assets/css/font-awesome.css" rel="stylesheet">
<link href='assets/css/font-family-righteous.css' rel='stylesheet'
	type='text/css'>
<link href="assets/plugins/fancybox/jquery.fancybox.css"
	rel="stylesheet">
<link href="assets/plugins/fullcalendar/fullcalendar.css"
	rel="stylesheet">
<link href="assets/plugins/xcharts/xcharts.min.css" rel="stylesheet">
<link href="assets/plugins/select2/select2.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">

<style>
.not-active {
	pointer-events: none;
	cursor: default;
}
</style>
</head>
<body>
	<!--Start Header-->
	<div id="screensaver">
		<canvas id="canvas"></canvas>
		<i class="fa fa-lock" id="screen_unlock"></i>
	</div>

	<div id="modalbox">
		<div class="devoops-modal">
			<div class="devoops-modal-header">
				<div class="modal-header-name">
					<span>Basic table</span>
				</div>
				<div class="box-icons">
					<a class="close-link"> <i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="devoops-modal-inner"></div>
			<div class="devoops-modal-bottom"></div>
		</div>
	</div>


	<header class="navbar">
	<div class="container-fluid expanded-panel">
		<div class="row">
			<div id="logo" class="col-xs-12 col-sm-2">
				<a href="<c:url value='/main' />">MPSoft&trade;</a>
			</div>
			<div id="top-panel" class="col-xs-12 col-sm-10">
				<div class="row">

					<div class="col-sm-5">
						<label>MPSoft&trade; Spring + Hibernate Framework
							Resources.</label>
					</div>

					<div class="col-xs-4 col-sm-7 top-panel-right">
						<ul class="nav navbar-nav pull-right panel-menu">

							<li class="dropdown"><a href="#"
								class="dropdown-toggle account" data-toggle="dropdown">
									<div class="avatar">
										<img src="assets/img/logojava.png" class="img-rounded"
											alt="avatar" />
									</div> <i class="fa fa-angle-down pull-right"></i>
									<div class="user-mini pull-right">
										<span class="welcome">WELCOME</span> <span>${SYSTEMUSER}</span>
									</div>
							</a>
								<ul class="dropdown-menu">
									<li><a href="#"> <i class="fa fa-cog"></i> <span>Settings</span></a></li>
									<li><a id="locked-screen" href=""><i
											class="fa fa-lock"></i><span>Lock Screen</span></a></li>
									<li><a href="<c:url value='/user-logout' />"> <i
											class="fa fa-power-off"></i> <span>Logout</span></a></li>
								</ul></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	</header>
	<!--End Header-->
	<!--Start Container-->
	<div id="main" class="container-fluid">
		<div class="row">
			<div id="sidebar-left" class="col-xs-2 col-sm-2">
				<ul class="nav main-menu">
					<li><a href="<c:url value='/dashboard' />"
						class="active ajax-link"> <i class="fa fa-dashboard"></i> <span
							class="hidden-xs">Dashboard</span></a></li>


					<li class="dropdown"><a href="#" id="idResources"
						class="dropdown-toggle"> <i class="fa fa-coffee"></i> <span
							class="hidden-xs">UI Resources</span>
					</a>
						<ul class="dropdown-menu">
							<li class="dropdown"><a href="#" class="dropdown-toggle">
									<i class="fa fa-bar-chart-o"></i> <span class="hidden-xs">Charts</span>
							</a>
								<ul class="dropdown-menu">
									<li><a class="ajax-link"
										href="assets/ajax/charts_xcharts.html">xCharts</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/charts_flot.html">Flot Charts</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/charts_google.html">Google Charts</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/charts_morris.html">Morris Charts</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/charts_coindesk.html">CoinDesk realtime</a></li>
								</ul></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle">
									<i class="fa fa-table"></i> <span class="hidden-xs">Tables</span>
							</a>
								<ul class="dropdown-menu">
									<li><a class="ajax-link"
										href="assets/ajax/tables_simple.html">Simple Tables</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/tables_datatables.html">Data Tables</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/tables_beauty.html">Beauty Tables</a></li>
								</ul></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle">
									<i class="fa fa-pencil-square-o"></i> <span class="hidden-xs">Forms</span>
							</a>
								<ul class="dropdown-menu">
									<li><a class="ajax-link"
										href="assets/ajax/forms_elements.html">Elements</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/forms_layouts.html">Layouts</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/forms_file_uploader.html">File Uploader</a></li>
								</ul></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle">
									<i class="fa fa-desktop"></i> <span class="hidden-xs">UI
										Elements</span>
							</a>
								<ul class="dropdown-menu">
									<li><a class="ajax-link" href="assets/ajax/ui_grid.html">Grid</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/ui_buttons.html">Buttons</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/ui_progressbars.html">Progress Bars</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/ui_jquery-ui.html">Jquery UI</a></li>
									<li><a class="ajax-link" href="assets/ajax/ui_icons.html">Icons</a></li>
								</ul></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle">
									<i class="fa fa-list"></i> <span class="hidden-xs">Pages</span>
							</a>
								<ul class="dropdown-menu">
									<li><a href="assets/ajax/page_login.html">Login</a></li>
									<li><a href="assets/ajax/page_register.html">Register</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/page_contacts.html">Contacts</a></li>
									<li><a class="ajax-link" href="assets/ajax/page_feed.html">Feed</a></li>
									<li><a class="ajax-link add-full"
										href="assets/ajax/page_messages.html">Messages</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/page_pricing.html">Pricing</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/page_invoice.html">Invoice</a></li>
									<li><a class="ajax-link"
										href="assets/ajax/page_search.html">Search Results</a></li>
									<li><a class="ajax-link" href="assets/ajax/page_404.html">Error
											404</a></li>
									<li><a href="assets/ajax/page_500.html">Error 500</a></li>
								</ul></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle">
									<i class="fa fa-map-marker"></i> <span class="hidden-xs">Maps</span>
							</a>
								<ul class="dropdown-menu">
									<li><a class="ajax-link" href="assets/ajax/maps.html">OpenStreetMap</a></li>
								</ul></li>
							<li><a class="ajax-link" href="assets/ajax/typography.html">
									<i class="fa fa-font"></i> <span class="hidden-xs">Typography</span>
							</a></li>
							<li><a class="ajax-link" href="assets/ajax/calendar.html">
									<i class="fa fa-calendar"></i> <span class="hidden-xs">Calendar</span>
							</a></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle">
									<i class="fa fa-picture-o"></i> <span class="hidden-xs">Multilevel
										menu</span>
							</a>
								<ul class="dropdown-menu">
									<li><a href="#">First level menu</a></li>
									<li><a href="#">First level menu</a></li>
									<li class="dropdown"><a href="#" class="dropdown-toggle">
											<i class="fa fa-plus-square"></i> <span class="hidden-xs">Second
												level menu group</span>
									</a>
										<ul class="dropdown-menu">
											<li><a href="#">Second level menu</a></li>
											<li><a href="#">Second level menu</a></li>
											<li class="dropdown"><a href="#" class="dropdown-toggle">
													<i class="fa fa-plus-square"></i> <span class="hidden-xs">Three
														level menu group</span>
											</a>
												<ul class="dropdown-menu">
													<li><a href="#">Three level menu</a></li>
													<li><a href="#">Three level menu</a></li>
													<li class="dropdown"><a href="#"
														class="dropdown-toggle"> <i class="fa fa-plus-square"></i>
															<span class="hidden-xs">Four level menu group</span></a>
														<ul class="dropdown-menu">
															<li><a href="#">Four level menu</a></li>
															<li><a href="#">Four level menu</a></li>
															<li class="dropdown"><a href="#"
																class="dropdown-toggle"> <i
																	class="fa fa-plus-square"></i> <span class="hidden-xs">Five
																		level menu group</span></a>
																<ul class="dropdown-menu">
																	<li><a href="#">Five level menu</a></li>
																	<li><a href="#">Five level menu</a></li>
																	<li class="dropdown"><a href="#"
																		class="dropdown-toggle"> <i
																			class="fa fa-plus-square"></i> <span
																			class="hidden-xs">Six level menu group</span>
																	</a>
																		<ul class="dropdown-menu">
																			<li><a href="#">Six level menu</a></li>
																			<li><a href="#">Six level menu</a></li>
																		</ul></li>
																</ul></li>
														</ul></li>
													<li><a href="#">Three level menu</a></li>
												</ul></li>
										</ul></li>
								</ul></li>
						</ul></li>

					<li class="dropdown"><a href="#" id="idCrud"
						class="dropdown-toggle"> <i class="fa fa-gears"></i> <span
							class="hidden-xs">CRUD Operations</span>
					</a>
						<ul class="dropdown-menu">
							<li><a class="ajax-link"
								href="<c:url value='/one-to-one' />">One to One</a></li>
							<li><a class="ajax-link"
								href="<c:url value='/one-to-many' />">One to Many</a></li>
							<li><a class="ajax-link"
								href="<c:url value='/many-to-many-simple' />">Many to Many</a></li>
						</ul></li>

					<li><a class="" target="_blank" id="idJasper"
						href="<c:url value='/report-view/category-report' />"> <i
							class="fa fa-book"></i> <span class="hidden-xs">Jasper
								Report View</span>
					</a> <%--       <ul class="dropdown-menu">
                            <li><a class="" target="_blank" href="<c:url value='/report-view/category-report' />">Simple Report</a></li>
                            <li><a class="ajax-link" href="assets/ajax/page_404.html">Report w/ sub-report</a></li>
                        </ul></li> --%>
					<li class="dropdown"><a href="#" id="idUtilities"
						class="dropdown-toggle"> <i class="fa fa-wrench"></i> <span
							class="hidden-xs">Utilities</span>
					</a>
						<ul class="dropdown-menu">
							<li><a class="ajax-link"
								href="<c:url value='/user-account' />">User Accounts</a></li>
							<li><a class="ajax-link" href="<c:url value='/user-level-setting' />">User
									Level Settings</a></li>
							<li><a class="ajax-link" href="<c:url value='/backup-restore' />">Database
									Backup & Restore</a></li>
						</ul></li>


				</ul>
			</div>
			<!--Start Content-->
			<div id="content" class="col-xs-12 col-sm-10">
				<div class="preloader">
					<img src="assets/img/devoops_getdata.gif" class="devoops-getdata"
						alt="preloader" />
				</div>
				<div id="ajax-content"></div>
			</div>
			<!--End Content-->
		</div>
	</div>
	<!--End Container-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="assets/plugins/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="assets/plugins/date.format.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
	<script
		src="assets/plugins/justified-gallery/jquery.justifiedgallery.min.js"></script>
	<script src="assets/plugins/tinymce/tinymce.min.js"></script>
	<script src="assets/plugins/tinymce/jquery.tinymce.min.js"></script>
	<!-- All functions for this theme + document.ready processing -->
	<script src="assets/js/devoops.js"></script>
	<script src="assets/js/ui-functions.js"></script>
	<script>
		$(document).ready(function() {
			$urlPrefix = "${urlPrefix}";
			$systemUser = "${SYSTEMUSER}";
		});
	</script>
	<script src="assets/js/main.js"></script>

</body>
</html>