<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmt:bundle basename="application">
	<fmt:message key="url.prefix" var="urlPrefix" />
	<fmt:message key="user.role" var="userRole" />
</fmt:bundle>

<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="main">Dashboard</a></li>
			<li><a href="#">Utilities</a></li>
			<li><a href="#">Database Backup & Restore</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-5 col-sm-offset-3">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span>Database Backup & Restore</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
					</a> <a class="expand-link"> <i class="fa fa-expand"></i>
					</a> <a class="close-link"> <i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" class="form-horizontal">
					<fieldset>
						<legend>Backup Database</legend>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button id="btnbackup" type="button"
									class="btn btn-success btn-block" onclick="doBackup()">Click
									here to backup</button>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Restore Database</legend>
						<div class="form-group">
							<label class="col-sm-2 control-label">Restore</label>
							<div class="col-sm-8">
								<select class="populate placeholder" name="restore" id="restore">
									<option value="">-- Select a database to restore --</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button id="btnrestore" type="submit"
									class="btn btn-danger btn-block">Click
									here to restore</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="assets/js/backup-restore.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$urlPrefix = "${urlPrefix}";
		$userRole = "${userRole}";
		$("#btncancel").click(function() {
			if (confirm("Cancel Transaction?")) {
				location.reload();
			}
		});
		LoadSelect2Script(selectSettings);
		// Add tooltip to form-controls
		$('.form-control').tooltip();
		LoadBootstrapValidatorScript(formValidations);
		// Add drag-n-drop feature to boxes
		WinMove();
		loadBackupFile();
	});
</script>

