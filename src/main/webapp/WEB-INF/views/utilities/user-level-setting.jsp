<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmt:bundle basename="application">
	<fmt:message key="url.prefix" var="urlPrefix" />
	<fmt:message key="user.role" var="userRole" />
</fmt:bundle>

<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="main">Dashboard</a></li>
			<li><a href="#">Utilities</a></li>
			<li><a href="#">User Level Settings</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-7 col-sm-offset-2">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span>User Level Settings</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
					</a> <a class="expand-link"> <i class="fa fa-expand"></i>
					</a> <a class="close-link"> <i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" class="form-horizontal">
					<fieldset>
						<legend>User Level Details</legend>
						<div class="form-group">
							<label class="col-sm-3 control-label">User Role</label>
							<div class="col-sm-6">
								<select class="populate placeholder" name="userRole"
									id="userRole">
									<option value="">-- Select a User Role --</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"></label>
							<div class="col-sm-9">
								<div class="checkbox">
									<label> <input type="checkbox" name="isActiveResources"
										id="isActiveResources" /> UI Resources <i class="fa fa-square-o small"></i>
									</label>
								</div>
							</div>
						</div>
							<div class="form-group">
							<label class="col-sm-3 control-label"></label>
							<div class="col-sm-9">
								<div class="checkbox">
									<label> <input type="checkbox" name="isActiveCrud"
										id="isActiveCrud" /> CRUD Operations <i class="fa fa-square-o small"></i>
									</label>
								</div>
							</div>
						</div>
							<div class="form-group">
							<label class="col-sm-3 control-label"></label>
							<div class="col-sm-9">
								<div class="checkbox">
									<label> <input type="checkbox" name="isActiveJasper"
										id="isActiveJasper" /> Jasper Report View <i class="fa fa-square-o small"></i>
									</label>
								</div>
							</div>
						</div>
							<div class="form-group">
							<label class="col-sm-3 control-label"></label>
							<div class="col-sm-9">
								<div class="checkbox">
									<label> <input type="checkbox" name="isActiveUtilities"
										id="isActiveUtilities" /> Utilities <i class="fa fa-square-o small"></i>
									</label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button id="btnsave" type="submit" class="btn btn-primary">Save</button>
								<button id="btncancel" type="button" class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="assets/js/user-level-setting.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$urlPrefix = "${urlPrefix}";
		$userRole = "${userRole}";
	   $("#btncancel").click(function() {
			if (confirm("Cancel Transaction?")) {
				location.reload();
			}
		});
		LoadSelect2Script(selectSettings);
		// Add tooltip to form-controls
		$('.form-control').tooltip();
		LoadBootstrapValidatorScript(formValidations);
		// Add drag-n-drop feature to boxes
		WinMove();
		populateUserRole();
		selectedRole();
		loadUserLevelList();
	});
</script>

