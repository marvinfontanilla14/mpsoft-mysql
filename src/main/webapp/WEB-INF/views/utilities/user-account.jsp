<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmt:bundle basename="application">
	<fmt:message key="url.prefix" var="urlPrefix" />
	<fmt:message key="user.role" var="userRole" />
</fmt:bundle>

<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="main">Dashboard</a></li>
			<li><a href="#">Utilities</a></li>
			<li><a href="#">User Account</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span>User Account</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
					</a> <a class="expand-link"> <i class="fa fa-expand"></i>
					</a> <a class="close-link"> <i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" class="form-horizontal">
					<fieldset>
						<legend>User Details</legend>
						<div class="form-group">
							<input type="hidden" name="userID" id="userID" /> <label
								class="col-sm-3 control-label">Username</label>
							<div class="col-sm-4">
								<input type="text" class="form-control account" name="userName"
									id="userName" maxlength="25" placeholder="Enter Username here"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Password</label>
							<div class="col-sm-4">
								<input type="password" class="form-control account" name="userPassword"
									id="userPassword" maxlength="25" placeholder="Enter Password here"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Confirm Password</label>
							<div class="col-sm-4">
								<input type="password" class="form-control" name="cuserPassword"
									id="cuserPassword" maxlength="25" placeholder="Enter Confirm Password here"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">User Role</label>
							<div class="col-sm-4">
								<select class="populate placeholder" name="userRole"
									id="userRole">
									<option value="">-- Select a User Role --</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">User Status</label>
							<div class="col-sm-9">
								<div class="checkbox">
									<label> <input type="checkbox" name="status"
										id="status" /> Active User <i class="fa fa-square-o small"></i>
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-9 col-sm-offset-3">
								<button id="btnsave" type="submit" class="btn btn-primary">Save</button>
								<button id="btncancel" type="button" class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>User Account List</legend>
						<div class="col-sm-9">
							<div class="box-content no-padding table-responsive">
								<table
									class="table table-bordered table-striped table-hover table-heading table-datatable"
									id="datatable">
									<thead>
										<tr>
											<th><label><input type="text"
													name="search_userName" value="Search User Name"
													class="search_init" /></label></th>
											<th><label>User Role</label></th>
											<th><label>Status</label></th>
											<th></th>
										</tr>
									</thead>

									<tbody id="table_data">

									</tbody>

									<tfoot>
										<tr>
											<th>User Name</th>
											<th>User Role</th>
											<th>Status</th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="assets/js/user-account.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$urlPrefix = "${urlPrefix}";
		$userRole = "${userRole}";
	    $("#status").prop('disabled', true);
	    $("#status").prop('checked', true);
		$("#btncancel").click(function() {
			if (confirm("Cancel Transaction?")) {
				location.reload();
			}
		});
		LoadSelect2Script(selectSettings);
		// Add tooltip to form-controls
		$('.form-control').tooltip();
		LoadBootstrapValidatorScript(formValidations);
		// Add drag-n-drop feature to boxes
		WinMove();
		loadTableData();
		LoadDataTablesScripts(loadTableSettings);
		populateUserRole();
	});
</script>

